export interface SelectListItem {
    value: number | string;
    text: string;
    checked?: boolean;
}

// RNI : Role, Name, Image

