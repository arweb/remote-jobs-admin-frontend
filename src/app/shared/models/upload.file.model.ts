export class UploadFileModel {
    fileName!: string
    size!: number
    url!: string
    identifier!: string
}


export class FileUploadInfoModel {
    public file!: File | undefined;
    public percentDone: number = 0
    public isShownProgressbar: boolean = false
    public filePath: string = "";
    public fileId: string = "";
}