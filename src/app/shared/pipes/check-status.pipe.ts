import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: 'checkStatus'
})
export class CheckStatusPipe implements PipeTransform {
    transform(status: boolean): any {
        let result = "radio_button_unchecked"
        if (status) result = "check_circle_outline";
        return result
    }
}