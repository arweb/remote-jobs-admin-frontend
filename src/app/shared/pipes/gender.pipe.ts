import { DomSanitizer } from '@angular/platform-browser'
import { PipeTransform, Pipe } from "@angular/core";
import { Gender } from '../enums/gender.enum';

@Pipe({ name: 'gender' })
export class GenderPipe implements PipeTransform {
    transform(value: number) {
        let result = "";
        switch (value) {
            case Gender.Man:
                result="مرد";
                break;

            case Gender.Woman:
                result="زن";
                break;

            case Gender.NotBigDeal:
                result="مهم نیست";
                break;
        }

        return result;
    }
}