import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'shamsiDateTime'
})
export class ShamsiDateTimePipe implements PipeTransform {
    transform(dateTime: Date): any {
        if (!dateTime) return ""

        const date = new Date(dateTime).toLocaleTimeString('fa-IR')
        const time = new Date(dateTime).toLocaleDateString('fa-IR')
        return `${date} - ${time}`
    }
}