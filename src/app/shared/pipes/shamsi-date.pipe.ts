import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'shamsiDate'
})
export class ShamsiDatePipe implements PipeTransform {
    transform(dateTime: Date): any {
        if (!dateTime) return ""

        const date = new Date(dateTime).toLocaleDateString('fa-IR')
        return `${date}`
    }
}