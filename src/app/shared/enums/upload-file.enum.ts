export enum UploadFileType {
    Logo = 1,
    Avatar = 2,
    Blog = 3,
    Resume = 4
}