import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './components/layout/layout.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyMaterialModule } from '../modules/my-material/my-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxSpinnerModule } from "ngx-spinner";
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { NoRecordComponent } from './components/no-record/no-record.component';
import { ShamsiDateTimePipe } from './pipes/shamsi-date-time.pipe';
import { ShamsiDatePipe } from './pipes/shamsi-date.pipe';
import { SafeHtmlPipe } from './pipes/safe-html-pipe';
import { SafeUrlPipe } from './pipes/safe-url.pipe';
import { CheckStatusPipe } from './pipes/check-status.pipe';
import { MessageDialogComponent } from './components/message-dialog/message-dialog.component';
import { GenderPipe } from './pipes/gender.pipe';


@NgModule({
  declarations: [
    LayoutComponent,
    ConfirmDialogComponent,
    NoRecordComponent,
    ShamsiDatePipe,
    ShamsiDateTimePipe,
    SafeHtmlPipe,
    SafeUrlPipe,
    CheckStatusPipe,
    MessageDialogComponent,
    GenderPipe,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    NgxSpinnerModule,
    MyMaterialModule,
  ],
  exports: [
    LayoutComponent,
    ConfirmDialogComponent,
    NoRecordComponent,
    ShamsiDatePipe,
    ShamsiDateTimePipe,
    MessageDialogComponent,

    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MyMaterialModule,
    SafeHtmlPipe,
    SafeUrlPipe,
    CheckStatusPipe,
    GenderPipe
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule,
      providers: [
      ]
    };
  }
}

