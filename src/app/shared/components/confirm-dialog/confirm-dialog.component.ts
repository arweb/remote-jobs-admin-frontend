import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  public header: string = "تائید عملیات"
  public caption: string = "آیا از انجام عملیات اطمینان دارید؟"
  public buttonOk: string = "بلی";
  public buttonCancel: string = "انصراف";

  constructor(
    private readonly _dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private readonly data: { header: string, caption: string, buttonOk: string, buttonCancel: string }) { }

  ngOnInit(): void {
    this.data?.header && (this.caption = this.data.header)
    this.data?.caption && (this.caption = this.data.caption)
    this.data?.buttonOk && (this.caption = this.data.buttonOk)
    this.data?.buttonCancel && (this.caption = this.data.buttonCancel)
  }

  submitConfirmation() {
    this._dialogRef.close()
  }

}
