import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-message-dialog',
  templateUrl: './message-dialog.component.html',
  styleUrls: ['./message-dialog.component.scss']
})
export class MessageDialogComponent implements OnInit {
  public messages: string[] = []

  constructor(
    private readonly _dialogRef: MatDialogRef<MessageDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private readonly data: string[]) { }

  ngOnInit(): void {
    this.data && Array.isArray(this.data) && this.data.length > 0 && (this.messages = this.data)
  }

  submitConfirmation() {
    this._dialogRef.close()
  }

}
