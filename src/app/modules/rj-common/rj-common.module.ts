import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RjCommonService } from './services/rj-common.service';
import { NumberOfStaffPipe } from './pipes/number-of-staff.pipe';
import { ActivityPipe } from './pipes/activity.pipe';
import { UserStatusPipe } from './pipes/user-status.pipe';


@NgModule({
  declarations: [
    ActivityPipe,
    NumberOfStaffPipe,
    UserStatusPipe,

  ],
  imports: [
    CommonModule
  ],
  providers: [
    RjCommonService
  ],
  exports: [
    ActivityPipe,
    NumberOfStaffPipe,
    UserStatusPipe,
  ]
})
export class RjCommonModule { }
