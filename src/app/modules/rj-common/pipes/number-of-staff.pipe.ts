import { Pipe, PipeTransform } from '@angular/core';
import { CompanyStaffCount } from '@app/features/employer/enums/company-staff-count.enum';



@Pipe({
    name: 'numberOfStaff'
})
export class NumberOfStaffPipe implements PipeTransform {
    transform(numberOfStaff: number): string {
        let result = ""

        switch (numberOfStaff) {
            case CompanyStaffCount.OneToTen:
                result = "از 1 - 10 نفر"
                break;

            case CompanyStaffCount.ElevenToTwenty:
                result = "از 11 - 20 نفر"
                break;

            case CompanyStaffCount.TwentyOneToFifty:
                result = "از 20 - 50 نفر"
                break;

            case CompanyStaffCount.FiftyOneToOneHundred:
                result = "از 50 - 100 نفر"
                break;

            case CompanyStaffCount.OneHundredOneToTwoHundred:
                result = "از 100 - 200 نفر"
                break;

            case CompanyStaffCount.TwoHundredOntToFiveHundred:
                result = "از 200 - 500 نفر"
                break;

            case CompanyStaffCount.MoreThanFiveHundred:
                result = "بیش از 500 نفر"
                break;
        }

        return result
    }
}