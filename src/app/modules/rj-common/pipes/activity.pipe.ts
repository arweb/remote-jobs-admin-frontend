import { Pipe, PipeTransform } from '@angular/core';
import { SelectListItem } from '@app/shared';

@Pipe({
    name: 'activity'
})
export class ActivityPipe implements PipeTransform {
    transform(activities: number[], activityItems: Array<SelectListItem>): string {
        let result = ''
        if (!activities) return result



        activities.map((activity, index) => {
            const { text } = activityItems.find(item => item.value == activity) as SelectListItem
            result += index == 0 ? text : `, ${text}`
        })

        return result
    }
}