import { Pipe, PipeTransform } from '@angular/core';
import { UserStatus } from '../enums/user-status.enum';

@Pipe({
    name: 'userStatus'
})
export class UserStatusPipe implements PipeTransform {
    transform(status: number, section: string = ""): string {
        let result = ""

        switch (status) {
            case UserStatus.Active:
                result = "فعال"
                break;
            case UserStatus.Deactive:
                result = "غیر فعال"
                break;

            case UserStatus.NotConfirmed:
                result = "تائید نشده"
                break;
        }

        return result
    }
}