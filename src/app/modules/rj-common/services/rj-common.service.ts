import { Injectable } from "@angular/core";
import { RoleType, SubRoleType } from "@app/core";
import { LoadingHttpRequest } from "@app/core/enums/show-loading-http-enum";
import { ApiHttpResponse } from "@app/core/models/api-http-response.model";
import { AppCrudService } from "@app/core/services/app-crud.service";
import { CompanyStaffCount } from "@app/features/employer/enums/company-staff-count.enum";
import { SelectListItem } from "@app/shared";
import { map, Observable } from "rxjs";
import { UserStatus } from "../enums/user-status.enum";

@Injectable()
export class RjCommonService {
    constructor(private readonly _appCrudService: AppCrudService) { }

    getActivityItems(): Observable<SelectListItem[]> {
        return this._appCrudService.getRequest(`management-activity/list/${1}`, undefined, LoadingHttpRequest.WithoutAnyLoading)
            .pipe(map((response: ApiHttpResponse<Array<any>>) => {
                return response?.data?.map(item => {
                    return { value: item.id, text: item.caption } as SelectListItem
                })
            }))
    }

    getNumberOfStaffItems() {
        const items: Array<SelectListItem> = []
        items.push({ value: CompanyStaffCount.OneToTen, text: 'از 1 تا 10 نفر' })
        items.push({ value: CompanyStaffCount.ElevenToTwenty, text: 'از 11 تا 20 نفر' })
        items.push({ value: CompanyStaffCount.TwentyOneToFifty, text: 'از 21 تا 50 نفر' })
        items.push({ value: CompanyStaffCount.FiftyOneToOneHundred, text: 'از 51 تا 100 نفر' })
        items.push({ value: CompanyStaffCount.OneHundredOneToTwoHundred, text: 'از 101 تا 200 نفر' })
        items.push({ value: CompanyStaffCount.TwoHundredOntToFiveHundred, text: 'از 201 تا 500 نفر' })
        items.push({ value: CompanyStaffCount.MoreThanFiveHundred, text: 'بیش از 500 نفر' })

        return items;
    }


    getUserStatus() {
        const items: Array<SelectListItem> = []
        items.push({ value: UserStatus.Active, text: 'فعال' })
        items.push({ value: UserStatus.Deactive, text: 'غیر فعال' })

        return items
    }


    getRoles() {
        const items: Array<SelectListItem> = []
        items.push({ value: SubRoleType.Master, text: 'مدیر ارشد' })
        items.push({ value: SubRoleType.SuperAdmin, text: 'مدیر' })
        items.push({ value: SubRoleType.Admin, text: 'پشتیبان' })
        return items
    }

}