export enum UserStatus {
    Active = 1,
    Deactive = 2,
    NotConfirmed = 3
}