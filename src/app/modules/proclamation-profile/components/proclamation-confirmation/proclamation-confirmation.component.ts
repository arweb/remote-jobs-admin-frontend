import { Component, Inject, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RjCommonService } from '@app/modules/rj-common';
import { SelectListItem } from '@app/shared/models/select-list-item-model';
import { ProclamationConfirmationModel } from '../../models/proclamation-confirmation.model';
import { ProclamationProfileService } from '../../services/proclamation-profile.service';

@Component({
  selector: 'app-proclamation-confirmation',
  templateUrl: './proclamation-confirmation.component.html',
  styleUrls: ['./proclamation-confirmation.component.scss']
})
export class ProclamationConfirmationComponent implements OnInit {

  public model!: ProclamationConfirmationModel
  public statusItems!: Array<SelectListItem>
  constructor(
    private readonly _dialogRef: MatDialogRef<ProclamationConfirmationComponent>,
    private readonly _proclamationService: ProclamationProfileService,
    @Inject(MAT_DIALOG_DATA) private readonly proclamationId: number
  ) { }

  ngOnInit(): void {
    this.init()
  }

  private async init() {
    this.model = new ProclamationConfirmationModel();
    this.statusItems = await this._proclamationService.getProclamationConfirmationStatus()
  }


  submitForm(form: NgForm, $event: Event) {
    $event.preventDefault();
    if (!form.valid) return;

    this._proclamationService.proclamationConfirmation(this.proclamationId, this.model).subscribe();
    this.onCloseDialog();
  }


  onCloseDialog() {
    this._dialogRef.close()
  }

}
