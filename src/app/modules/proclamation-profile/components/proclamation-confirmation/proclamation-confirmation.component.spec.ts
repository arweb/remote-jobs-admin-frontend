import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProclamationConfirmationComponent } from './proclamation-confirmation.component';

describe('ProclamationConfirmationComponent', () => {
  let component: ProclamationConfirmationComponent;
  let fixture: ComponentFixture<ProclamationConfirmationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProclamationConfirmationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProclamationConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
