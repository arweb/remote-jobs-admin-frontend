import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProclamationProfileActionComponent } from './proclamation-profile-action.component';

describe('ProclamationProfileActionComponent', () => {
  let component: ProclamationProfileActionComponent;
  let fixture: ComponentFixture<ProclamationProfileActionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProclamationProfileActionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProclamationProfileActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
