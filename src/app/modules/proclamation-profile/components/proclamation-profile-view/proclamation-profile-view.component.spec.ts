import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProclamationProfileViewComponent } from './proclamation-profile-view.component';

describe('ProclamationProfileViewComponent', () => {
  let component: ProclamationProfileViewComponent;
  let fixture: ComponentFixture<ProclamationProfileViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProclamationProfileViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProclamationProfileViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
