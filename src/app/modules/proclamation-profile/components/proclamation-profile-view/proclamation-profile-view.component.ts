import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProclamationModel } from '@app/modules/proclamation-common';
import { ProclamationProfileService } from '../../services/proclamation-profile.service';
import { finalize } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { ProclamationConfirmationComponent } from '../proclamation-confirmation/proclamation-confirmation.component';
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-proclamation-profile-view',
  templateUrl: './proclamation-profile-view.component.html',
  styleUrls: ['./proclamation-profile-view.component.scss']
})
export class ProclamationProfileViewComponent implements OnInit {

  public model!: ProclamationModel
  constructor(
    private readonly _route: ActivatedRoute,
    private readonly _router: Router,
    public readonly _dialog: MatDialog,
    private readonly _spinner: NgxSpinnerService,
    private readonly _proclamationProfileService: ProclamationProfileService) { }

  ngOnInit(): void {
    this.init();
    this.getDataItem();
  }

  private init() {
    this.model = new ProclamationModel();
  }

  private getDataItem() {
    const id = this._route.parent?.parent?.snapshot.params['id']
    this._proclamationProfileService.getProclamation(id)
      .pipe(
        finalize(() => {
          this._spinner.hide();
        }))
      .subscribe((response) => {
        this.model = response;
      });
  }


  public openProclamationConfirmation(){
    const id = this._route.parent?.parent?.snapshot.params['id']
    this._dialog.open(ProclamationConfirmationComponent, { data:id, disableClose: true, panelClass: 'modal-sm' });
  }
}
