import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { ProclamationModel } from '@app/modules/proclamation-common';
import { HistoryProclamationDto } from '../../models/proclamation-history.model';
import { ProclamationProfileService } from '../../services/proclamation-profile.service';
import { HistoryDetailComponent } from '../history-detail/history-detail.component';

@Component({
  selector: 'app-proclamation-profile-history',
  templateUrl: './proclamation-profile-history.component.html',
  styleUrls: ['./proclamation-profile-history.component.scss']
})
export class ProclamationProfileHistoryComponent implements OnInit {
  public displayedColumns: string[] = [
    "id",
    "categoryMode",
    "creationDate",
    "message",
    "status",
    "action"
  ];

  public items!: Array<HistoryProclamationDto>
  constructor(
    private readonly _route: ActivatedRoute,
    public readonly _dialog: MatDialog,
    private readonly _proclamationService: ProclamationProfileService,
  ) { }

  ngOnInit(): void {
    this.init();

    this.getDataItem();
  }

  private init() {
    this.items = []
  }

  private getDataItem() {
    const id = this._route.parent?.parent?.snapshot.params['id']
    this._proclamationService.getProclamationHistory(id).subscribe(response => {
      this.items = response


      console.log(this.items)
    })
  }

  public showHostiryItem({ id }: { id: number }) {
    const { history } = this.items.find(item => item.id == id) as HistoryProclamationDto
    const proclamation = JSON.parse(history) as ProclamationModel


    this._dialog.open(HistoryDetailComponent, { data: proclamation, disableClose: true, panelClass: 'modal-lg' });
  }
}
