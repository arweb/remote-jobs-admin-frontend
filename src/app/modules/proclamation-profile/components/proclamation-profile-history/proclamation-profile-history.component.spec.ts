import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProclamationProfileHistoryComponent } from './proclamation-profile-history.component';

describe('ProclamationProfileHistoryComponent', () => {
  let component: ProclamationProfileHistoryComponent;
  let fixture: ComponentFixture<ProclamationProfileHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProclamationProfileHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProclamationProfileHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
