import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProclamationProfileLayoutComponent } from './proclamation-profile-layout.component';

describe('ProclamationProfileLayoutComponent', () => {
  let component: ProclamationProfileLayoutComponent;
  let fixture: ComponentFixture<ProclamationProfileLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProclamationProfileLayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProclamationProfileLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
