import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProclamationModel } from '@app/modules/proclamation-common';

@Component({
  selector: 'app-history-detail',
  templateUrl: './history-detail.component.html',
  styleUrls: ['./history-detail.component.scss']
})
export class HistoryDetailComponent implements OnInit {

  public model!: ProclamationModel
  constructor(
    private readonly _dialogRef: MatDialogRef<HistoryDetailComponent>,
    @Inject(MAT_DIALOG_DATA) private readonly data: ProclamationModel
  ) { }

  ngOnInit(): void {
    this.model = this.data
  }

  onCloseDialog() {
    this._dialogRef.close()
  }

}
