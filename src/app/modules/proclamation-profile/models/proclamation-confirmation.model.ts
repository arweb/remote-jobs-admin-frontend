import { ProclamationHistoryStatus } from "@app/modules/proclamation-common/enums/proclamation-history-status.enum"

export class ProclamationConfirmationModel{
    status!: ProclamationHistoryStatus
    message!: string
}