import { HistoryCategoryMode, ProclamationHistoryStatus } from "@app/modules/proclamation-common"

export class HistoryProclamationDto {
    public id!: number
    public history!: string
    public creationDate!: Date
    public message!: string
    public status!: ProclamationHistoryStatus
    public isParent!: boolean
    public categoryMode!: HistoryCategoryMode
}

