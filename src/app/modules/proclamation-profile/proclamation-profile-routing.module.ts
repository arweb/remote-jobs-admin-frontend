import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProclamationProfileActionComponent } from './components/proclamation-profile-action/proclamation-profile-action.component';
import { ProclamationProfileHistoryComponent } from './components/proclamation-profile-history/proclamation-profile-history.component';
import { ProclamationProfileLayoutComponent } from './components/proclamation-profile-layout/proclamation-profile-layout.component';
import { ProclamationProfileViewComponent } from './components/proclamation-profile-view/proclamation-profile-view.component';

const routes: Routes = [
  {
    path: "", component: ProclamationProfileLayoutComponent, children: [
      {
        path: "", redirectTo: "view", pathMatch: "full"
      },
      {
        path: "view", component: ProclamationProfileViewComponent
      },
      {
        path: "actioin", component: ProclamationProfileActionComponent
      },
      {
        path: "history", component: ProclamationProfileHistoryComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProclamationProfileRoutingModule { }
