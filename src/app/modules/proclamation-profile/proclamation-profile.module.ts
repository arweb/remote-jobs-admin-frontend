import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProclamationProfileRoutingModule } from './proclamation-profile-routing.module';
import { ProclamationProfileLayoutComponent } from './components/proclamation-profile-layout/proclamation-profile-layout.component';
import { ProclamationProfileViewComponent } from './components/proclamation-profile-view/proclamation-profile-view.component';
import { ProclamationProfileActionComponent } from './components/proclamation-profile-action/proclamation-profile-action.component';
import { ProclamationProfileHistoryComponent } from './components/proclamation-profile-history/proclamation-profile-history.component';
import { SharedModule } from '@app/shared';
import { ProclamationCommonModule } from '../proclamation-common';
import { ProclamationConfirmationComponent } from './components/proclamation-confirmation/proclamation-confirmation.component';
import { HistoryDetailComponent } from './components/history-detail/history-detail.component';


@NgModule({
  declarations: [
    ProclamationProfileLayoutComponent,
    ProclamationProfileViewComponent,
    ProclamationProfileActionComponent,
    ProclamationProfileHistoryComponent,
    ProclamationConfirmationComponent,
    HistoryDetailComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ProclamationCommonModule,
    ProclamationProfileRoutingModule
  ]
})
export class ProclamationProfileModule { }
