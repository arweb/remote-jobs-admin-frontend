import { Injectable } from "@angular/core";
import { LoadingHttpRequest } from "@app/core/enums/show-loading-http-enum";
import { ApiHttpResponse } from "@app/core/models/api-http-response.model";
import { AppCrudService } from "@app/core/services/app-crud.service";
import { ProclamationModel } from "@app/modules/proclamation-common";
import { ProclamationHistoryStatus } from "@app/modules/proclamation-common/enums/proclamation-history-status.enum";
import { SelectListItem } from "@app/shared";
import { map, Observable } from "rxjs";
import { ProclamationConfirmationModel } from "../models/proclamation-confirmation.model";
import { HistoryProclamationDto } from "../models/proclamation-history.model";

@Injectable({ providedIn: "root" })
export class ProclamationProfileService {
    constructor(private readonly _appCrudService: AppCrudService) { }
    getProclamation(proclamationId: number): Observable<ProclamationModel> {
        return this._appCrudService.getRequest("proclamation/item", proclamationId, LoadingHttpRequest.WithoutAnyLoading)
            .pipe(map((response: ApiHttpResponse<ProclamationModel>) => {
                return response.data
            }))
    }

    getProclamationConfirmationStatus(): Promise<Array<SelectListItem>> {
        return new Promise((resolve, rejct) => {
            const items: Array<SelectListItem> = []

            items.push({ value: ProclamationHistoryStatus.Confirmed, text: 'تائید شده' });
            items.push({ value: ProclamationHistoryStatus.Rejected, text: 'رد شده' });
            items.push({ value: ProclamationHistoryStatus.Suspend, text: 'معلق' });

            resolve(items)
        })
    }


    proclamationConfirmation(proclamationId: number, model: ProclamationConfirmationModel) {
        return this._appCrudService.putRequest("proclamation/confirmation", model, proclamationId, LoadingHttpRequest.WithoutAnyLoading)
    }



    getProclamationHistory(proclamationId: number): Observable<Array<HistoryProclamationDto>> {
        return this._appCrudService.getRequest("proclamation/history-item", proclamationId, LoadingHttpRequest.WithoutAnyLoading)
            .pipe(map((response: ApiHttpResponse<Array<HistoryProclamationDto>>) => {
                return response.data
            }))
    }
}