import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployerProfileRoutingModule } from './employer-profile-routing.module';
import { EmployerOverviewComponent } from './components/employer-overview/employer-overview.component';
import { EmployerActionComponent } from './components/employer-action/employer-action.component';
import { EmployerProclamationComponent } from './components/employer-proclamation/employer-proclamation.component';
import { EmployerProfileLayoutComponent } from './components/employer-profile-layout/employer-profile-layout.component';
import { SharedModule } from '@app/shared';
import { RjCommonModule } from '../rj-common';


@NgModule({
  declarations: [
    EmployerOverviewComponent,
    EmployerActionComponent,
    EmployerProclamationComponent,
    EmployerProfileLayoutComponent
  ],
  imports: [
    CommonModule,
    RjCommonModule,
    SharedModule,
    EmployerProfileRoutingModule
  ]
})
export class EmployerProfileModule { }
