import { Inject, Injectable } from "@angular/core";
import { LoadingHttpRequest } from "@app/core/enums/show-loading-http-enum";
import { AppCrudService } from "@app/core/services/app-crud.service";
import { CompanyFindDto } from "@app/features/employer/models/company-find.dto";

@Injectable({ providedIn: "root" })
export class EmployerActionService {
    constructor(private readonly _appCrudService: AppCrudService) { }

    // updateCompanyNameAndNationalId
    submitEmployerNameAndNatinalId(companyId: number, model: CompanyFindDto) {
        return this._appCrudService.putRequest("company", model, companyId, LoadingHttpRequest.WithoutAnyLoading)
    }
}