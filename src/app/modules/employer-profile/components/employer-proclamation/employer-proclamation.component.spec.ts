import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerProclamationComponent } from './employer-proclamation.component';

describe('EmployerProclamationComponent', () => {
  let component: EmployerProclamationComponent;
  let fixture: ComponentFixture<EmployerProclamationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployerProclamationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployerProclamationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
