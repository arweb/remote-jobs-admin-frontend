import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { CompanyFindDto } from '@app/features/employer/models/company-find.dto';
import { EmployerService } from '@app/features/employer/services/employer.service';
import { RjCommonService } from '@app/modules/rj-common';
import { ConfirmDialogComponent, SelectListItem } from '@app/shared';


@Component({
  selector: 'app-employer-overview',
  templateUrl: './employer-overview.component.html',
  styleUrls: ['./employer-overview.component.scss']
})
export class EmployerOverviewComponent implements OnInit {

  public model!: CompanyFindDto
  public activityItems!: Array<SelectListItem>

  constructor(
    private readonly _router: Router,
    private readonly _route: ActivatedRoute,
    private readonly _employerService: EmployerService,
    private readonly _commonService: RjCommonService,
    public readonly _dialog: MatDialog,) { }

  ngOnInit(): void {
    this.init();
    this.getActivityItems();
    this.getDataItem();
  }

  private init() {
    this.model = {} as CompanyFindDto;
    this.activityItems = []
  }

  private getActivityItems() {
    this._commonService.getActivityItems().subscribe((response: Array<SelectListItem>) => {
      this.activityItems = response
    })
  }

  private getDataItem() {
    const userId = +this._route.parent?.parent?.snapshot.params['id']
    this._employerService.getCompanyInfo(userId).subscribe(response => {
      this.model = response
    })
  }

}
