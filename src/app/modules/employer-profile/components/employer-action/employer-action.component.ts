import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CompanyFindDto } from '@app/features/employer/models/company-find.dto';
import { EmployerService } from '@app/features/employer/services/employer.service';
import { RjCommonService } from '@app/modules/rj-common';
import { SelectListItem } from '@app/shared';
import { EmployerActionService } from '../../services/employer-action.service';

@Component({
  selector: 'app-employer-action',
  templateUrl: './employer-action.component.html',
  styleUrls: ['./employer-action.component.scss']
})
export class EmployerActionComponent implements OnInit {
  public model!: CompanyFindDto
  public selected: number[] = []
  public activityItems!: Array<SelectListItem>
  public numberOfStaffItems!: Array<SelectListItem>

  constructor(
    private readonly _employerActionService: EmployerActionService,
    private readonly _route: ActivatedRoute,
    private readonly _commonService: RjCommonService,
    private readonly _employerService: EmployerService,) { }

  ngOnInit(): void {
    this.init();

    this.getNumberOfStaff();
    this.getActivityItems();
    this.getDataItem();
  }

  private init() {
    this.model = new CompanyFindDto();
    this.activityItems = []
    this.numberOfStaffItems = []
  }

  private getNumberOfStaff() {
    this.numberOfStaffItems = this._commonService.getNumberOfStaffItems();
  }

  private getActivityItems() {
    this._commonService.getActivityItems().subscribe((response: Array<SelectListItem>) => {
      this.activityItems = response
    })
  }

  private getDataItem() {
    const userId = +this._route.parent?.parent?.snapshot.params['id']
    this._employerService.getCompanyInfo(userId).subscribe((response: CompanyFindDto) => {
      this.model = response
    })
  }

  public submit(form: NgForm, $event: Event) {
    $event.preventDefault();

    if (!form.valid) return;
    this._employerActionService.submitEmployerNameAndNatinalId(this.model.id, this.model).subscribe();
  }

}
