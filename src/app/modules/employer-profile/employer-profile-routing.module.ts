import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployerActionComponent } from './components/employer-action/employer-action.component';
import { EmployerOverviewComponent } from './components/employer-overview/employer-overview.component';
import { EmployerProclamationComponent } from './components/employer-proclamation/employer-proclamation.component';
import { EmployerProfileLayoutComponent } from './components/employer-profile-layout/employer-profile-layout.component';

const routes: Routes = [
  {
    path: "", component: EmployerProfileLayoutComponent, children: [
      {
        path: "", redirectTo: "overview", pathMatch: "full"
      },
      {
        path: "overview", component: EmployerOverviewComponent
      },
      {
        path: "proclamation", component: EmployerProclamationComponent
      },
      {
        path: "action", component: EmployerActionComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployerProfileRoutingModule { }
