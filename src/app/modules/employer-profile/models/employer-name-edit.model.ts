export class EmployerNameEditModel {
    public companyId!: number
    public englishName!: string;
    public persianName!: string;
    public companyNationalId!: string;

    public constructor(init?: Partial<EmployerNameEditModel>) {
        Object.assign(this, init);
    }
}