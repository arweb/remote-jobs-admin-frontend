import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';
import { SeniorityLevvelPipe } from './pipes/seniority-level.pipe';
import { SallerylPipe } from './pipes/sallery.pipe';
import { ProclamationStatusPipe } from './pipes/proclamation-status.pipe';
import { EducationTypePipe } from './pipes/education-type.pipe';
import { CollbrationTypePipe } from './pipes/collabration-type.pipe';
import { ProclamationTypePipe } from './pipes/proclamation-type.pipe';
import { ProclamationHistoryStatusPipe } from './pipes/proclamation-history-status.pipe';
import { HistoryCategoryPipe } from './pipes/history-category.pipe';



@NgModule({
  declarations: [
    CollbrationTypePipe,
    EducationTypePipe,
    ProclamationStatusPipe,
    SallerylPipe,
    SeniorityLevvelPipe,
    ProclamationTypePipe,
    ProclamationHistoryStatusPipe,
    HistoryCategoryPipe
  ],
  imports: [
    SharedModule,
    CommonModule
  ],
  exports: [
    CollbrationTypePipe,
    EducationTypePipe,
    ProclamationStatusPipe,
    SallerylPipe,
    SeniorityLevvelPipe,
    ProclamationTypePipe,
    ProclamationHistoryStatusPipe,
    HistoryCategoryPipe
  ]
})
export class ProclamationCommonModule { }
