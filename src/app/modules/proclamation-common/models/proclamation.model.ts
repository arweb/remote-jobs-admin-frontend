import { CollabrationType } from "../enums/collabration-type.enum";
import { EducationType } from "../enums/education-type.enum";
import { ProclamationStatus } from "../enums/proclamation-status.enum";
import { ProclamationType } from "../enums/proclamation-type.enum";
import { Sallery } from "../enums/sallery.enum";

export class ProclamationModel{
    public id!: number;
    public caption!: string
    public collabration!: CollabrationType
    public minimumSallery!: Sallery
    public isEnglishContext!: string
    public minimumEducation!: EducationType
    public gender!: number
    public status!: ProclamationStatus
    public type!: ProclamationType
    public categoryId!: number
    public categoryCaption!: string
    public requirements!: string
    public responsibilities!: string
    public benefits!: string
    public seniorityLevel!: number


    
    public creationDate!: Date
    public updatedDate!: Date
    public expireDate!: Date
}