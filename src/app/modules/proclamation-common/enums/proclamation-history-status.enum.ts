export enum ProclamationHistoryStatus {
    Suspend = 1,
    Rejected = 2,
    Confirmed = 3
}