export enum Sallery {
    From4Million = 4,
    From5Million = 5,
    From7Million = 7,
    From10Million = 10,
    From12Million = 12,
    From15Million = 15,
    From20Million = 20,
    From30Million = 30,
    From40Million = 40,
    From50Million = 50
}