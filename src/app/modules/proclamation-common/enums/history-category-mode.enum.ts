export enum HistoryCategoryMode {
    Unpublished = 1,
    Published = 2
}