export enum ProclamationStatus {
    Suspend = 1,
    Confirmed = 2,
    Active = 3,
    Closed = 4,
    Draft = 5,
}