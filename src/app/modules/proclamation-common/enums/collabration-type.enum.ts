export enum CollabrationType {
    Fulltime = 1,
    Parttime = 2
}