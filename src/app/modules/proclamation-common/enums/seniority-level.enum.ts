export enum SeniorityLevel {
    Fresh_Graduate = 1,
    Expert = 2,
    Experienced = 3,
    Mid_Level_Managemnet = 4,
    Top_Management = 5
}