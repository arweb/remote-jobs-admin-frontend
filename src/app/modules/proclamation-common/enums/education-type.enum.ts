export enum EducationType {
    UnderDiploma = 1,
    Diploma = 2,
    AssociateDegree = 3,
    Bachelor = 4,
    Master = 5,
    Doctorate = 6,
    NotBigDeal = 50
}