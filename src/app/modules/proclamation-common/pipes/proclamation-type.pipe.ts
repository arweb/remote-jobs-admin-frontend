import { Pipe, PipeTransform } from '@angular/core';
import { ProclamationType } from '../enums/proclamation-type.enum';

@Pipe({
    name: 'proclamationType'
})
export class ProclamationTypePipe implements PipeTransform {
    transform(value: number): string {
        let result = "";

        switch (value) {
            case ProclamationType.Emergent:
                result = "فوری";
                break;

            case ProclamationType.Inemergent:
                result = "معمولی";
                break;
        }

        return result;
    }
}