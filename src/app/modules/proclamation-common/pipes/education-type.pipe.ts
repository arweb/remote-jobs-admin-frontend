import { Pipe, PipeTransform } from '@angular/core';
import { EducationType } from '../enums/education-type.enum';

@Pipe({
    name: 'educationType'
})
export class EducationTypePipe implements PipeTransform {
    transform(value: number): string {
        let result = "";

        switch (value) {
            case EducationType.AssociateDegree:
                result = "فوق دیپلم";
                break;

            case EducationType.Bachelor:
                result = "لیسانس";
                break;

            case EducationType.Diploma:
                result = "دیپلم";
                break;


            case EducationType.Doctorate:
                result = "دکتری";
                break;


            case EducationType.Master:
                result = "فوق لیسانس";
                break;


            case EducationType.NotBigDeal:
                result = "مهم نیست";
                break;


            case EducationType.UnderDiploma:
                result = "زیر دیپلم";
                break;
        }

        return result;
    }
}