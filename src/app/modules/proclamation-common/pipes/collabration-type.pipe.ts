import { Pipe, PipeTransform } from '@angular/core';
import { CollabrationType } from '../enums/collabration-type.enum';

@Pipe({
    name: 'collbrationType'
})
export class CollbrationTypePipe implements PipeTransform {
    transform(value: number): string {
        let result = "";

        switch (value) {
            case CollabrationType.Fulltime:
                result = "تمام وقت";
                break;

            case CollabrationType.Parttime:
                result = "نیمه وقت";
                break;
        }

        return result;
    }
}