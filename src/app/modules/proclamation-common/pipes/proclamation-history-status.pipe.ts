import { Pipe, PipeTransform } from '@angular/core';
import { ProclamationHistoryStatus } from '../enums/proclamation-history-status.enum';

@Pipe({
    name: 'proclamationHistoryStatus'
})
export class ProclamationHistoryStatusPipe implements PipeTransform {
    transform(value: number): string {
        let result = "";

        switch (value) {
            case ProclamationHistoryStatus.Confirmed:
                result = "تائید شده";
                break;

            case ProclamationHistoryStatus.Rejected:
                result = "رد شده";
                break;

            case ProclamationHistoryStatus.Suspend:
                result = "معلق";
                break;
        }

        return result;
    }
}