import { Pipe, PipeTransform } from '@angular/core';
import { ProclamationStatus } from '../enums/proclamation-status.enum';

@Pipe({
    name: 'proclamationStatus'
})
export class ProclamationStatusPipe implements PipeTransform {
    transform(value: number): string {
        let result = "";

        switch (value) {
            case ProclamationStatus.Active:
                result = "فعال";
                break;

            case ProclamationStatus.Closed:
                result = "بسته شده";
                break;

            case ProclamationStatus.Confirmed:
                result = "تائید شده";
                break;

            case ProclamationStatus.Draft:
                result = "پیش نویس";
                break;

            case ProclamationStatus.Suspend:
                result = "معلق";
                break;
        }

        return result;
    }
}