import { Pipe, PipeTransform } from '@angular/core';
import { Sallery } from '../enums/sallery.enum';

@Pipe({
    name: 'sallery'
})
export class SallerylPipe implements PipeTransform {
    transform(value: number): string {
        let result = "";

        switch (value) {
            case Sallery.From4Million:
                result = "از 4 ملیون تومان";
                break;

            case Sallery.From5Million:
                result = "از 5 ملیون تومان";
                break;

            case Sallery.From7Million:
                result = "از 7 ملیون تومان";
                break;

            case Sallery.From10Million:
                result = "از 10 ملیون تومان";
                break;

            case Sallery.From12Million:
                result = "از 12 ملیون تومان";
                break;

            case Sallery.From15Million:
                result = "از 15 ملیون تومان";
                break;

            case Sallery.From20Million:
                result = "از 20 ملیون تومان";
                break;

            case Sallery.From30Million:
                result = "از 30 ملیون تومان";
                break;

            case Sallery.From40Million:
                result = "از 40 ملیون تومان";
                break;

            case Sallery.From50Million:
                result = "از 50 ملیون تومان";
                break;
        }

        return result;
    }
}