import { Pipe, PipeTransform } from '@angular/core';
import { SeniorityLevel } from '../enums/seniority-level.enum';


@Pipe({
    name: 'seniorityLevvel'
})
export class SeniorityLevvelPipe implements PipeTransform {
    transform(value: number): string {
        let result = "";

        switch (value) {
            case SeniorityLevel.Experienced:
                result="کارشناس ارشد";
                break;

            case SeniorityLevel.Expert:
                result="کارشناس";
                break;

            case SeniorityLevel.Fresh_Graduate:
                result="تازه کار";
                break;

            case SeniorityLevel.Mid_Level_Managemnet:
                result="سرپرست/مدیر";
                break;

            case SeniorityLevel.Top_Management:
                result="مدیر ارشد";
                break;
        }

        return result;
    }
}