import { Pipe, PipeTransform } from '@angular/core';
import { HistoryCategoryMode } from '../enums/history-category-mode.enum';
import { ProclamationHistoryStatus } from '../enums/proclamation-history-status.enum';

@Pipe({
    name: 'historyCategory'
})
export class HistoryCategoryPipe implements PipeTransform {
    transform(value: number): string {
        let result = "";

        switch (value) {
            case HistoryCategoryMode.Published:
                result = "تغیرات برای آگهی در وضعیت انتشار می باشد.";
                break;

            case HistoryCategoryMode.Unpublished:
                result = "تغیرات برای آگهی که هنور انتشار نیافته است می باشد.";
                break;
        }

        return result;
    }
}