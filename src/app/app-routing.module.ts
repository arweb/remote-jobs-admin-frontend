import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard, RoleType } from './core';
import { AuthGuardPermission } from './core/models/auth-guard-permission';
import { LayoutComponent } from './shared/components/layout/layout.component';

const routes: Routes = [
  {
    path: "", redirectTo: "dashboard", pathMatch: "full"
  },
  {
    path: "",
    component: LayoutComponent,
    children: [{ path: "dashboard", loadChildren: () => import('./features/dashboard/dashboard.module').then(m => m.DashboardModule) }],
    data:
    {
      permission: {
        permittedRoles: [RoleType.Management]
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },
  {
    path: "employers", component: LayoutComponent, children: [{
      path: "", loadChildren: () => import('./features/employer/employer.module').then(m => m.EmployerModule),
    }],
    data:
    {
      permission: {
        permittedRoles: [RoleType.Management]
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },
  {
    path: "job-seekers", component: LayoutComponent, children: [{
      path: "", loadChildren: () => import('./features/job-seeker/job-seeker.module').then(m => m.JobSeekerModule),
    }],
    data:
    {
      permission: {
        permittedRoles: [RoleType.Management]
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },
  {
    path: "users", component: LayoutComponent, children: [{
      path: "", loadChildren: () => import('./features/user/user.module').then(m => m.UserModule),
    }],
    data:
    {
      permission: {
        permittedRoles: [RoleType.Management]
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },
  {
    path: "proclamations", component: LayoutComponent, children: [{
      path: "", loadChildren: () => import('./features/proclamation/proclamation.module').then(m => m.ProclamationModule),
    }],
    data:
    {
      permission: {
        permittedRoles: [RoleType.Management]
      } as AuthGuardPermission
    },
    canActivate: [AuthGuard]
  },
  {
    path: "auth", loadChildren: () => import('./features/auth/auth.module').then(m => m.AuthModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
