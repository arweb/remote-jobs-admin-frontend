import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, tap } from 'rxjs';
import { MySnackbarService } from '../services/snackbar.service';
import { ApiHttpResponse } from '../models/api-http-response.model';
import { MatDialog } from '@angular/material/dialog';
import { MessageDialogComponent } from '@app/shared';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(
        public readonly _dialog: MatDialog,
        private readonly _snackbarService: MySnackbarService) { }


    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(tap({
            next: (response) => {
                if (response instanceof HttpResponse) {
                    const result = response.body as ApiHttpResponse<string>
                    this.showToasterMessage(result)
                    this.showMessageDialog(result)
                }
            },
            error: (error) => {
                if (error instanceof HttpErrorResponse) {
                    const result = error.error as ApiHttpResponse<string>
                    this.showToasterMessage(result)
                    this.showMessageDialog(result)
                }
            }
        }))
    }


    private showToasterMessage = (result: ApiHttpResponse<any>) => result && result?.message && result?.type &&
        this._snackbarService.showMessage(result.message, result.type)

    private showMessageDialog = (result: ApiHttpResponse<any>) => result && result.messages && result.messages.length > 0 &&
        this._dialog.open(MessageDialogComponent, { panelClass: "modal-sm", data: result.messages, disableClose: false })

}
