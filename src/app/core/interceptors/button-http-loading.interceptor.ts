import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CustomHttpParams } from '../models/custom-http-header';
import { LoadingHttpRequest } from '../enums/show-loading-http-enum';
import { BusyService } from '../services/busy.service';

@Injectable()
export class ButtonHttpLoadingInterceptor implements HttpInterceptor {

  constructor(private busyService: BusyService) { }

  private callBusyService(event: HttpEvent<any>, id: string = "rqb-1") {
    if ((event.type === 0)) {
      this.busyService.changeBusy('busy', id);
    } else {
      this.busyService.changeBusy('not busy', id);
    }
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let btnId: any = null

    return next.handle(req).pipe(tap(event => {

      const loadingHttpRequest = req.params instanceof CustomHttpParams ? req.params.loadingHttpRequest : ''

      switch (loadingHttpRequest) {
        case LoadingHttpRequest.RequesterButton:
          btnId = "rqb-1"
          this.callBusyService(event, btnId)
          break;

        case LoadingHttpRequest.RequesterButton2:
          btnId = "rqb-2"
          this.callBusyService(event, btnId)
          break;

        case LoadingHttpRequest.RequesterButton3:
          btnId = "rqb-3"
          this.callBusyService(event, btnId)
          break;

        case LoadingHttpRequest.RequesterButton4:
          btnId = "rqb-4"
          this.callBusyService(event, btnId)
          break;

        case LoadingHttpRequest.RequesterButton5:
          btnId = "rqb-5"
          this.callBusyService(event, btnId)
          break;

        case LoadingHttpRequest.RequesterButton6:
          btnId = "rqb-6"
          this.callBusyService(event, btnId)
          break;

        case LoadingHttpRequest.RequesterButton7:
          btnId = "rqb-7"
          this.callBusyService(event, btnId)
          break;

        case LoadingHttpRequest.RequesterButton8:
          btnId = "rqb-8"
          this.callBusyService(event, btnId)
          break;

        case LoadingHttpRequest.RequesterButton9:
          btnId = "rqb-9"
          this.callBusyService(event, btnId)
          break;

        case LoadingHttpRequest.RequesterButton10:
          btnId = "rqb-10"
          this.callBusyService(event, btnId)
          break;

        default:
          break;
      }

    }, error => {
      this.busyService.changeBusy('not busy', btnId);
    }));
  }
}
