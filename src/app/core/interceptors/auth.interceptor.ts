import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AuthTokenType } from "../enums/auth-token-type";
import { TokenStoreService } from "./../services/token-store.service";


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  private authorizationHeader = "Authorization";

  constructor(private tokenStoreService: TokenStoreService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const accessToken = this.tokenStoreService.getRawAuthToken(AuthTokenType.AccessToken);
    if (accessToken)
      request = request.clone({
        headers: request.headers.set(this.authorizationHeader, `Bearer ${accessToken}`)
      });
    return next.handle(request);
  }

}
