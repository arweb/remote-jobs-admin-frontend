export enum RoleType {
    Management = 1,
    Employer = 2,
    JobSeeker = 3  // For Job Seeker Section 
}

export enum SubRoleType {
    Master = 1, // For Admin Section
    SuperAdmin = 2, // For Admin Section : With Limitation Access
    Admin = 3, // For Admin Section : With Limitatin Access
    Company = 4,// For Company Section
    CompanyMember = 5, // For Company Section : With Limitation Access
    JobSeeker = 6
}
