// درخواست کننده گرید و درخواست کننده مدال
export enum LoadingHttpRequest {
    ModalRequester = 1,
    UsualRequester = 2,
    RequesterButton = 3,
    GridRequester = 4,
    FormRequester = 5,
    DisplayRequester = 6,
    WithoutAnyLoading = 7,

    RequesterButton2 = 8,
    RequesterButton3 = 9,
    RequesterButton4 = 10,
    RequesterButton5 = 11,
    RequesterButton6 = 12,
    RequesterButton7 = 13,
    RequesterButton8 = 14,
    RequesterButton9 = 15,
    RequesterButton10 = 16,


}