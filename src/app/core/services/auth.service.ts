import { HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, throwError } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { TokenStoreService } from "./token-store.service";
import { AuthUser } from '../models/auth-user';
import { AppCrudService } from "./app-crud.service";
import { LoadingHttpRequest } from "../enums/show-loading-http-enum";
import { Router } from "@angular/router";
import { HeaderItem } from "../models/header-item.model";
import { RefreshTokenService } from "./refresh-token.service";
import { captchaToken } from "../models/captach-site.key";
import { AuthTokenType } from "../enums/auth-token-type";
import { ApiHttpResponse } from "../models/api-http-response.model";
import { RoleType } from "../enums/role-type.enum";


@Injectable({ providedIn: 'root' })
export class AuthService {

    private authStatusSource = new BehaviorSubject<boolean>(false);
    authStatus$ = this.authStatusSource.asObservable();

    constructor(
        private _router: Router,
        private _appCrudService: AppCrudService,
        private _tokenStoreService: TokenStoreService,
        private _refreshTokenService: RefreshTokenService
    ) {
        this.updateStatusOnPageRefresh();
        this._refreshTokenService.scheduleRefreshToken(this.isAuthUserLoggedIn(), false);
    }
    login(model: any): Observable<boolean> {
        const headerItems: HeaderItem[] = [{ key: captchaToken, value: model.captcha }]

        return this._appCrudService.postRequest(model, "auth/login", undefined, LoadingHttpRequest.RequesterButton, false)
            .pipe(
                map((response: ApiHttpResponse<string>) => {
                    if (!response.data) {
                        this.authStatusSource.next(false);
                        return false;
                    }

                    this._tokenStoreService.storeLoginSession(response.data);
                    this._refreshTokenService.scheduleRefreshToken(true, true);
                    this.authStatusSource.next(true);
                    return true

                }),
                catchError((error: HttpErrorResponse) => throwError(error))
            )
    }

    reLogin(response: any) {
        this._tokenStoreService.storeLoginSession(response);
        this._refreshTokenService.scheduleRefreshToken(true, true);
        this.authStatusSource.next(true);
    }

    logout() {
        // send request to server to delete refresh token .....
        this._tokenStoreService.deleteAuthTokens();
        this._refreshTokenService.unscheduleRefreshToken(true);
        this.authStatusSource.next(false);
        this._router.navigate(["/auth/login"]);
    }

    getBearerAuthHeader(): HttpHeaders {
        return new HttpHeaders({
            "Content-Type": "application/json",
            "Authorization": `Bearer ${this._tokenStoreService.getRawAuthToken(AuthTokenType.AccessToken)}`
        });
    }

    isAuthUserLoggedIn(): boolean {
        return this._tokenStoreService.hasStoredAccess() && !this._tokenStoreService.isAccessTokenTokenExpired();
    }



    getAuthUser(): Readonly<AuthUser> | null {
        if (!this.isAuthUserLoggedIn())
            return null;

        const decodedToken = this._tokenStoreService.getDecodedAccessToken();
        return Object.freeze({
            email: decodedToken['email'],
            firstName: decodedToken["firstName"],
            lastName: decodedToken["lastName"],
            userId: decodedToken["userId"],
            companyId: decodedToken["companyId"],
            status: decodedToken["status"],
            isVerified: decodedToken["isVerified"],
            role: decodedToken["role"],
            subRole: decodedToken["subRole"]
        });
    }

    public getUserDisplayName() {
        const { firstName, lastName } = this.getAuthUser() as Readonly<AuthUser>
        return `${firstName} ${lastName}`
    }

    isAuthUserInRoles(requiredRoles: RoleType[]): boolean {
        const user = this.getAuthUser();
        if (!user || !user.role) {
            return false;
        }

        if (user.role == RoleType.Management) {
            return true; // The `Master` role has full access to every pages.
        }

        return requiredRoles.some(requiredRole => {
            return user.role == requiredRole ? true : false;
        });
    }

    broadecasteLogout() {
        this.authStatusSource.next(false)
    }

    isAuthUserInRole(requiredRole: RoleType): boolean {
        return this.isAuthUserInRoles([requiredRole]);
    }


    private updateStatusOnPageRefresh(): void {
        this.authStatusSource.next(this.isAuthUserLoggedIn());
    }



    redirectToCorrectModuleBasedOnRole() {
        // const { role } = this.getAuthUser() as Readonly<AuthUser>
        // switch (role) {
        //     case RoleType.Company:
        //     case RoleType.CompanyMember:
        //         this._router.navigate(['employer-panel'])
        //         break;


        //     case RoleType.JobSeeker:
        //         this._router.navigate(['job-seeker-panel'])
        //         break;
        // }

        this._router.navigate(['/dashboard'])
    }

}
