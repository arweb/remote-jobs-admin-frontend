﻿import { InjectionToken } from '@angular/core';
import { environment } from 'environments/environment';


export let APP_CONFIG = new InjectionToken<string>('app.config');

export interface IAppConfig {
  apiEndpoint: string,
  fileServer: string,
  production: boolean
}

export const AppConfig: IAppConfig = {
  apiEndpoint: environment.apiEndpoint,
  fileServer: environment.fileServer,
  production: environment.production
};

