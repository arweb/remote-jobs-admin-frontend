import { Injectable } from "@angular/core";
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from "@angular/material/snack-bar";
import { Toaster } from "../enums/toaster-type.enum";


@Injectable({ providedIn: "root" })
export class MySnackbarService {
    private readonly _durationInSeconds!: number;
    private readonly _horizontalPosition!: MatSnackBarHorizontalPosition;
    private readonly _verticalPosition!: MatSnackBarVerticalPosition;

    constructor(private readonly _snackBar: MatSnackBar) {
        this._durationInSeconds = 4000;
        this._horizontalPosition = 'center';
        this._verticalPosition = 'bottom';
    }

    showMessage(message: string, type: Toaster) {
        this._snackBar.open(message, '', {
            horizontalPosition: this._horizontalPosition,
            verticalPosition: this._verticalPosition,
            duration: this._durationInSeconds
        })
    }
}
