import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { SelectListItem } from '@app/shared';

@Injectable({ providedIn: 'root' })
export class AppCommonService {

    constructor(
        @Inject(PLATFORM_ID) private readonly _platformId: any) { }


    public isBrowserPlatform = () => isPlatformBrowser(this._platformId)
    public isServerPlatform = () => isPlatformServer(this._platformId)



    public timeout(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    public getConfigOfCKEditor(plugins: string[] = []): any {
        const removeButtons: string = 'Source,Templates,Save,NewPage,Print,Replace,Scayt,SelectAll,Form,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Blockquote,CreateDiv,Language,Image,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,ShowBlocks,About,Checkbox,Find,Preview,Styles,Format,Anchor,Maximize,CopyFormating';
        const config = {
            allowedContent: false,
            contentsLangDirection: 'rtl',
            forcePasteAsPlainText: false,
            extraPlugins: `${plugins ? plugins.toString() : ''}`, //strinsert
            removeButtons: removeButtons
        }

        return config
    }


    public convertEnumToSelectListItem(e: { [s: number]: string }): SelectListItem[] {
        const selectList: SelectListItem[] = [];
        for (const enumMember in e)
            if (parseInt(enumMember, 10) >= 0) {
                const text = e[enumMember].split('_').join(" ");
                selectList.push({ value: parseInt(enumMember, 10), text: text, checked: false });
            }

        return selectList;
    }

    public removeHyphenInUrlSubject(subject: string) {
        const format = (subject: any) =>
            subject
                .trim()
                .toLowerCase()
                .replaceAll('/', ' ')
                .replaceAll('-', ' ')
                .replace(/\s\s+/g, ' ')
                .split(/\s|%20/)
                .filter(Boolean)
                .join("-");
        return format(subject);
    }

}