import { Injectable } from "@angular/core";
import jwt_decode from 'jwt-decode';
import { AuthTokenType } from "../enums/auth-token-type";

import { BrowserStorageService } from "./browser-storage.service";
import { UtilService } from "./util.service";

@Injectable({
  providedIn: 'root'
})
export class TokenStoreService {

  private rememberMeToken = "rememberMe_token";

  constructor(
    private readonly browserStorageService: BrowserStorageService,
    private readonly _utilService: UtilService) { }

  getRawAuthToken(tokenType: AuthTokenType): string {
    return this.browserStorageService.getLocal(AuthTokenType[tokenType]);
  }

  getDecodedAccessToken(): any {
    return jwt_decode(this.getRawAuthToken(AuthTokenType.AccessToken));
  }

  getAuthUserDisplayName(): string {
    return this.getDecodedAccessToken().DisplayName;
  }

  getAccessTokenExpirationDateUtc(): Date | null {
    const decoded = this.getDecodedAccessToken();
    if (decoded.exp === undefined) {
      return null;
    }
    const date = new Date(0); // The 0 sets the date to the epoch
    date.setUTCSeconds(decoded.exp);
    return date;
  }

  getAccessTokenExpiretionDate() {
    const decoded = this.getDecodedAccessToken();
    if (decoded.exp === undefined) {
      return null;
    }
    return decoded.exp
  }

  isAccessTokenTokenExpired(): boolean {
    const expirationDateUtc = this.getAccessTokenExpirationDateUtc();
    if (!expirationDateUtc) {
      return true;
    }
    return !(expirationDateUtc.valueOf() > new Date().valueOf());
  }

  deleteAuthTokens() {
    this.browserStorageService.removeLocal(AuthTokenType[AuthTokenType.AccessToken]);
    this.browserStorageService.removeLocal(AuthTokenType[AuthTokenType.RefreshToken]);
    this.browserStorageService.removeLocal(this.rememberMeToken);
  }

  setToken(tokenType: AuthTokenType, tokenValue: string): void {
    if (this._utilService.isEmptyString(tokenValue)) {
      console.error(`${AuthTokenType[tokenType]} is null or empty.`);
    }

    if (tokenType === AuthTokenType.AccessToken && this._utilService.isEmptyString(tokenValue)) {
      throw new Error("AccessToken can't be null or empty.");
    }
    this.browserStorageService.setLocal(AuthTokenType[tokenType], tokenValue);
  }


  storeLoginSession(response: any): void {
    this.setToken(AuthTokenType.AccessToken, response["accessToken"]);
    this.setToken(AuthTokenType.RefreshToken, response["refreshToken"]);
  }

  hasStoredAccess(): boolean {
    const accessToken = this.getRawAuthToken(AuthTokenType.AccessToken);
    const refreshToken = this.getRawAuthToken(AuthTokenType.RefreshToken);
    return !this._utilService.isEmptyString(accessToken) && !this._utilService.isEmptyString(refreshToken);
  }
}
