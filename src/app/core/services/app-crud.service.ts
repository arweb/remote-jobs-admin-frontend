import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs";
import { CustomHttpParams } from '../models/custom-http-header';
import { IAppConfig, APP_CONFIG } from './app.config';
import { LoadingHttpRequest } from '../enums/show-loading-http-enum';
import { HeaderItem } from '../models/header-item.model';


@Injectable({ providedIn: "root" })
export class AppCrudService {

    constructor(
        private http: HttpClient,
        @Inject(APP_CONFIG) private appConfig: IAppConfig) { }


    public getRequest(url: string, id?: any, loadingHttp: LoadingHttpRequest = LoadingHttpRequest.UsualRequester): Observable<any> {
        const overalUrl = id ? `${this.appConfig.apiEndpoint}/${url}/${id}` : `${this.appConfig.apiEndpoint}/${url}`;
        return this.http.get(overalUrl, this.setHttpHeader(loadingHttp));
    }

    public getRequestWithQuerystring(url: string, query: any, id?: any, loadingHttp: LoadingHttpRequest = LoadingHttpRequest.UsualRequester): Observable<any> {
        const overalUrl = id ? `${this.appConfig.apiEndpoint}/${url}/${id}/${query}` : `${this.appConfig.apiEndpoint}/${url}/${query}`;
        return this.http.get(overalUrl, this.setHttpHeader(loadingHttp));
    }

    public getRequestWithModel(url: string, model: any, id?: any, loadingHttp: LoadingHttpRequest = LoadingHttpRequest.UsualRequester): Observable<any> {

        let params = new URLSearchParams();
        for (let key in model) {
            params.set(key, model[key])
        }

        let overalUrl = id ? `${this.appConfig.apiEndpoint}/${url}/${id}` : `${this.appConfig.apiEndpoint}/${url}`;
        overalUrl = params ? `${overalUrl}?${params}` : `${overalUrl}`;
        return this.http.get(overalUrl, this.setHttpHeader(loadingHttp));
    }

    public postRequest<T>(model: T, url: string, id?: any, loadingHttp: LoadingHttpRequest = LoadingHttpRequest.UsualRequester, formData = false, headers: HeaderItem[] = []): Observable<any> {
        const overalUrl = id ? `${this.appConfig.apiEndpoint}/${url}/${id}` : `${this.appConfig.apiEndpoint}/${url}`;
        return this.http.post(overalUrl, formData ? this.getFormData(model) : model, this.setHttpHeader(loadingHttp, headers, formData));
    }


    public putRequest<T>(url: string, model: T, id?: any, loadingHttp: LoadingHttpRequest = LoadingHttpRequest.UsualRequester, formData = false): Observable<any> {
        const overalUrl = id ? `${this.appConfig.apiEndpoint}/${url}/${id}` : `${this.appConfig.apiEndpoint}/${url}`;
        return this.http.put(overalUrl, formData ? this.getFormData(model) : model, this.setHttpHeader(loadingHttp, [], formData));
    }

    public patchRequest<T>(url: string, model: T, id?: any, loadingHttp: LoadingHttpRequest = LoadingHttpRequest.UsualRequester, formData = false): Observable<any> {
        const overalUrl = id ? `${this.appConfig.apiEndpoint}/${url}/${id}` : `${this.appConfig.apiEndpoint}/${url}`;
        return this.http.patch(overalUrl, formData ? this.getFormData(model) : model, this.setHttpHeader(loadingHttp, [], formData));
    }

    public patchRequestWithMutiParameters(url: string, id: any[], loadingHttp: LoadingHttpRequest = LoadingHttpRequest.UsualRequester): Observable<any> {
        let obtained = '';
        id.forEach(item => {
            obtained += item + '/';
        });

        const overalUrl = `${this.appConfig.apiEndpoint}/${url}/${obtained}`;

        return this.http.patch(overalUrl, {}, this.setHttpHeader(loadingHttp));
    }

    public patchRequestWithMutiParametersAndModel(url: string, model: any, id: any[], loadingHttp: LoadingHttpRequest = LoadingHttpRequest.UsualRequester): Observable<any> {
        let obtained = '';
        id.forEach(item => {
            obtained += item + '/';
        });

        const overalUrl = `${this.appConfig.apiEndpoint}/${url}/${obtained}`;

        return this.http.patch(overalUrl, model, this.setHttpHeader(loadingHttp));
    }

    public postRequestWithMutiParameters(url: string, id: any[], loadingHttp: LoadingHttpRequest = LoadingHttpRequest.RequesterButton, model: any = {}): Observable<any> {
        let obtained = '';
        id.forEach(item => {
            obtained += item + '/';
        });

        const overalUrl = `${this.appConfig.apiEndpoint}/${url}/${obtained}`;

        return this.http.post(overalUrl, model, this.setHttpHeader(loadingHttp));
    }

    public patchRequestById(id: any, url: string, loadingHttp: LoadingHttpRequest = LoadingHttpRequest.UsualRequester): Observable<any> {
        const overalUrl = `${this.appConfig.apiEndpoint}/${url}/${id}`;
        return this.http.patch(overalUrl, {}, this.setHttpHeader(loadingHttp));
    }

    public deleteRequest(id: any, url: string, loadingHttp: LoadingHttpRequest = LoadingHttpRequest.UsualRequester): Observable<any> {
        return this.http.delete(`${this.appConfig.apiEndpoint}/${url}/${id}`, this.setHttpHeader(loadingHttp));
    }

    public getRequestWithMutiParameters(url: string, id: any[], loadingHttp: LoadingHttpRequest = LoadingHttpRequest.UsualRequester): Observable<any> {

        let obtained = '';
        id.forEach(item => {
            obtained += item + '/';
        });

        const overalUrl = `${this.appConfig.apiEndpoint}/${url}/${obtained}`;
        return this.http.get(overalUrl, this.setHttpHeader(loadingHttp));
    }



    private setHttpHeader(loadingHttp: LoadingHttpRequest, items: HeaderItem[] = [], formData: boolean = false) {

        const response: any = {}
        items.forEach(item => {
            const { 0: key, 1: value } = Object.values(item)
            response[key] = value
        })

        let headers = new HttpHeaders(response).set('Accept', 'application/json')
        if (formData) headers.set('Content-Type', 'application/json')


        let options = { headers: headers, params: new CustomHttpParams(loadingHttp) }
        return options;
    }

    private getFormData(object: any) {
        const formData = new FormData();

        Object.keys(object).forEach(key => {
            formData.append(key, Array.isArray(object[key]) ? JSON.stringify(object[key]) : object[key])
        });
        return formData;
    }

}

