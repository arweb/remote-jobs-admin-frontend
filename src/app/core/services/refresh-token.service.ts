import { HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Subscription, throwError, timer } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { AuthTokenType } from "../enums/auth-token-type";
import { LoadingHttpRequest } from "../enums/show-loading-http-enum";
import { ApiHttpResponse } from "../models/api-http-response.model";
import { AppCommonService } from "./app-common.service";
import { AppCrudService } from "./app-crud.service";
import { BrowserStorageService } from "./browser-storage.service";
import { TokenStoreService } from "./token-store.service";
import { UtilService } from "./util.service";


@Injectable({ providedIn: 'root' })
export class RefreshTokenService {

  private refreshTokenTimerCheckId = "is_refreshToken_timer_started";
  private refreshTokenSubscription: Subscription | null = null;

  constructor(
    private readonly appCrudService: AppCrudService,
    private readonly tokenStoreService: TokenStoreService,
    private readonly browserStorageService: BrowserStorageService,
    private readonly _utilService:UtilService) { }

  scheduleRefreshToken(isAuthUserLoggedIn: boolean, calledFromLogin: boolean) {
    this.unscheduleRefreshToken(false);

    if (!isAuthUserLoggedIn) {
      return;
    }

    const expires = new Date(this.tokenStoreService.getAccessTokenExpiretionDate() * 1000);
    if (!expires) {
      throw new Error("This access token has not the `exp` property.");
    }



    const initialDelay = expires.getTime() - Date.now() - (60 * 1000);
    const timerSource$ = timer(initialDelay);
    this.refreshTokenSubscription = timerSource$.subscribe(() => {
      if (calledFromLogin || !this.isRefreshTokenTimerStartedInAnotherTab()) {
        this.refreshToken(isAuthUserLoggedIn);
      } else {
        this.scheduleRefreshToken(isAuthUserLoggedIn, false);
      }
    });

    if (calledFromLogin || !this.isRefreshTokenTimerStartedInAnotherTab()) {
      this.setRefreshTokenTimerStarted();
    }
  }

  unscheduleRefreshToken(cancelTimerCheckToken: boolean) {
    if (this.refreshTokenSubscription) {
      this.refreshTokenSubscription.unsubscribe();
    }

    if (cancelTimerCheckToken) {
      this.deleteRefreshTokenTimerCheckId();
    }
  }

  invalidateCurrentTabId() {
    const currentTabId = this._utilService.getCurrentTabId();
    const timerStat = this.browserStorageService.getLocal(this.refreshTokenTimerCheckId);
    if (timerStat && timerStat.tabId === currentTabId) {
      this.setRefreshTokenTimerStopped();
    }
  }

  private refreshToken(isAuthUserLoggedIn: boolean) {
    const model = { refreshToken: this.tokenStoreService.getRawAuthToken(AuthTokenType.RefreshToken) };

    return this.appCrudService.postRequest(model, "auth/connect/token", undefined, LoadingHttpRequest.WithoutAnyLoading)
      .pipe(
        map((response: ApiHttpResponse<{ accessToken: string, refreshToken: string }>) => response || {}),
        catchError((error: HttpErrorResponse) => throwError(error))
      )
      .subscribe((result: ApiHttpResponse<{ accessToken: string, refreshToken: string }>) => {
        this.tokenStoreService.storeLoginSession(result.data);
        this.scheduleRefreshToken(isAuthUserLoggedIn, false);
      })
  }

  private isRefreshTokenTimerStartedInAnotherTab(): boolean {
    const currentTabId = this._utilService.getCurrentTabId();
    const timerStat = this.browserStorageService.getLocal(this.refreshTokenTimerCheckId);
    const isStarted = timerStat && timerStat.isStarted === true && timerStat.tabId !== currentTabId;
    return isStarted;
  }

  private setRefreshTokenTimerStarted(): void {
    this.browserStorageService.setLocal(this.refreshTokenTimerCheckId,
      {
        isStarted: true,
        tabId: this._utilService.getCurrentTabId()
      });
  }

  private deleteRefreshTokenTimerCheckId() {
    this.browserStorageService.removeLocal(this.refreshTokenTimerCheckId);
  }

  private setRefreshTokenTimerStopped(): void {
    this.browserStorageService.setLocal(this.refreshTokenTimerCheckId, {
      isStarted: false,
      tabId: -1
    });
  }
}
