import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: "root" })
export class BusyService {
  // Observable navItem source
  private _busySource = new BehaviorSubject<any>(null);
  // Observable navItem stream
  busy$ = this._busySource.asObservable();

  constructor() { }

  changeBusy(val: string, btnId: string) {
    this._busySource.next({ status: val, btn: btnId });
  }

}
