import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ButtonHttpLoadingInterceptor } from './interceptors/button-http-loading.interceptor';
import { AppConfig, APP_CONFIG } from './services/app.config';
import { ErrorInterceptor } from './interceptors/error-interceptor';
import { AuthInterceptor } from './interceptors/auth.interceptor';


@NgModule({
  providers:
    [
      {
        provide: HTTP_INTERCEPTORS,
        useClass: AuthInterceptor,
        multi: true
      },
      {
        provide: HTTP_INTERCEPTORS,
        useClass: ButtonHttpLoadingInterceptor,
        multi: true
      },
      {
        provide: HTTP_INTERCEPTORS,
        useClass: ErrorInterceptor,
        multi: true
      },
      {
        provide: APP_CONFIG,
        useValue: AppConfig
      },
    ],
  imports:
    [
      CommonModule
    ]
})
export class CoreModule { }
