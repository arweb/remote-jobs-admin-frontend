export interface AuthUser {
  email: string
  firstName: string
  lastName: string
  userId: number
  companyId?: number
  companyName?: string
  role: number
  subRole: number
  status: number
  isVerified?: boolean
}
