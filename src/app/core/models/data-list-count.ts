export class DataListAndCount<T> {
    constructor(public items: Array<T>, public count: number) { }
}