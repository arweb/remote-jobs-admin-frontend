export interface DataSourceRequestState {
    pageNumber: number;
    pageSize: number;
}