export interface AuthGuardPermission {
    permittedRoles?: number[];
    deniedRoles?: number[];
  }
  