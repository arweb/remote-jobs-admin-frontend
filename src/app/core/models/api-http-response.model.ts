import { Toaster } from "../enums/toaster-type.enum";

const SUCCESS_MESSAGE = "عملیات با موفقیت صورت گرفت.";

export class ApiHttpResponse<T>{
    constructor(
        public readonly data: T,
        public readonly message: string = SUCCESS_MESSAGE,
        public readonly type: Toaster = Toaster.Success,
        public readonly isSuccess: boolean = true,
        public readonly messages: string[] = []
    ) { }
}
