import { HttpParams } from '@angular/common/http';
import { LoadingHttpRequest } from '../enums/show-loading-http-enum';

export class CustomHttpParams extends HttpParams {
    constructor(
        public loadingHttpRequest: LoadingHttpRequest) {
        super();
    }
}
