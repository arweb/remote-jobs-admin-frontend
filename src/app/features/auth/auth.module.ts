import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginIndexComponent } from './components/login-index/login-index.component';
import { ForgotIndexComponent } from './components/forgot-index/forgot-index.component';
import { AuthLayoutComponent } from './components/auth-layout/auth-layout.component';
import { SharedModule } from '@app/shared';
import { MyMaterialModule } from '@app/modules/my-material';


@NgModule({
  declarations: [
    LoginIndexComponent,
    ForgotIndexComponent,
    AuthLayoutComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MyMaterialModule,
    AuthRoutingModule
  ]
})
export class AuthModule { }
