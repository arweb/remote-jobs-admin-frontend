import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthLayoutComponent } from './components/auth-layout/auth-layout.component';
import { ForgotIndexComponent } from './components/forgot-index/forgot-index.component';
import { LoginIndexComponent } from './components/login-index/login-index.component';

const routes: Routes = [
  {
    path: "", component: AuthLayoutComponent, children: [
      {
        path: "", redirectTo: "login", pathMatch: "full"
      },
      {
        path: "login", component: LoginIndexComponent
      },
      {
        path: "forgot", component: ForgotIndexComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
