import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForgotIndexComponent } from './forgot-index.component';

describe('ForgotIndexComponent', () => {
  let component: ForgotIndexComponent;
  let fixture: ComponentFixture<ForgotIndexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForgotIndexComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgotIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
