import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { AuthService } from '@app/core/services/auth.service';

@Component({
  selector: 'app-login-index',
  templateUrl: './login-index.component.html',
  styleUrls: ['./login-index.component.scss']
})
export class LoginIndexComponent implements OnInit {

  loginForm!: FormGroup;
  loading!: boolean;

  constructor(
    private readonly _router: Router,
    private readonly _authService: AuthService,
  ) {
  }

  ngOnInit() {
    this.createForm();
  }

  private createForm() {
    const savedUserEmail = localStorage.getItem('savedUserEmail');

    this.loginForm = new FormGroup({
      email: new FormControl(savedUserEmail, [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
    });
  }

  login() {
    const email = this.loginForm.get('email')?.value;
    const password = this.loginForm.get('password')?.value;

    this.loading = true;

    this._authService.login({ email, password }).subscribe(_ => {
      this._authService.redirectToCorrectModuleBasedOnRole()
    })

  }

  resetPassword() {
   //  this._router.navigate(['/auth/password-reset-request']);
  }

}
