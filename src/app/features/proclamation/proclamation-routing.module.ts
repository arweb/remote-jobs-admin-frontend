import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProclamationEditedListComponent } from './components/proclamation-edited-list/proclamation-edited-list.component';
import { ProclamationListComponent } from './components/proclamation-list/proclamation-list.component';
import { ProclamationNotConfirmedListComponent } from './components/proclamation-not-confirmed-list/proclamation-not-confirmed-list.component';

const routes: Routes = [
  {
    path: "list", component: ProclamationListComponent
  },
  {
    path: "edited-list", component: ProclamationEditedListComponent
  },
  {
    path: "not-confirmed-list", component: ProclamationNotConfirmedListComponent
  },
  {
    path: "profile/:id", loadChildren: () => import('../../modules/proclamation-profile/proclamation-profile.module').then(m => m.ProclamationProfileModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProclamationRoutingModule { }
