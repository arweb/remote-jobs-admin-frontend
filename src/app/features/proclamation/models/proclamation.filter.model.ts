import { DataSourceRequestState } from "@app/core/models/data-source-request-state";

export class ProclamationFilterModel{
    isExpired!:boolean;
    englishName!: string;
    persianName!: string;
    collabration!: number;
    minimumSallery!: number;
    minimumEducation!: number;
    status!: number;
    seniorityLevel!:number;
    categoryId!:number;
}

export class DataSourceProclamationFilterRequest extends ProclamationFilterModel implements DataSourceRequestState {
    public pageNumber!: number;
    public pageSize!: number;
}