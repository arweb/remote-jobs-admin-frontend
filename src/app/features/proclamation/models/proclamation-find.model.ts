export class ProclamationFindModel {
    public id!: number
    public caption!: string
    public categoryCaption!: string
    public creationDate!: Date
    public expireDate!: Date
    public companyPersianName!: string
}