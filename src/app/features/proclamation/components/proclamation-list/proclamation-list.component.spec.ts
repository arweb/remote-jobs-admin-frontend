import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProclamationListComponent } from './proclamation-list.component';

describe('ProclamationListComponent', () => {
  let component: ProclamationListComponent;
  let fixture: ComponentFixture<ProclamationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProclamationListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProclamationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
