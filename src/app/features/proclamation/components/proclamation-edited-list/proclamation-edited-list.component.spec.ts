import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProclamationEditedListComponent } from './proclamation-edited-list.component';

describe('ProclamationEditedListComponent', () => {
  let component: ProclamationEditedListComponent;
  let fixture: ComponentFixture<ProclamationEditedListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProclamationEditedListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProclamationEditedListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
