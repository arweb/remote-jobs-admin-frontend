import { Component, OnInit, ViewChild } from '@angular/core';
import { DataListAndCount } from '@app/core/models/data-list-count';
import { NgxSpinnerService } from 'ngx-spinner';
import { ProclamationFindModel } from '../../models/proclamation-find.model';
import { DataSourceProclamationFilterRequest } from '../../models/proclamation.filter.model';
import { ProclamationService } from '../../services/proclamation.service';
import { finalize } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-proclamation-not-confirmed-list',
  templateUrl: './proclamation-not-confirmed-list.component.html',
  styleUrls: ['./proclamation-not-confirmed-list.component.scss']
})
export class ProclamationNotConfirmedListComponent implements OnInit {

  public displayedColumns: string[] = [
    "id",
    "caption",
    "categoryCaption",
    "creationDate",
    "expireDate",
    "companyPersianName",
    "action"
  ];


  public dataItems: ProclamationFindModel[] = [];
  public state!: DataSourceProclamationFilterRequest;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    private readonly _route: ActivatedRoute,
    private readonly _router: Router,
    private readonly _spinner: NgxSpinnerService,
    private readonly _proclamationService: ProclamationService) { }

  ngOnInit(): void {
    this.init();

    this.getDataTable();
  }

  private init() {
    this.state = { pageNumber: 0, pageSize: 15 } as DataSourceProclamationFilterRequest;
  }

  private getDataTable() {
    this._spinner.show();
    this._proclamationService.getNotConfrimedList(this.state)
      .pipe(
        finalize(() => {
          this._spinner.hide();
        }))
      .subscribe((response: DataListAndCount<ProclamationFindModel>) => {
        const { count, items } = response;
        this.paginator.length = count;
        this.dataItems = items;
      });
  }

  changePaging({ pageIndex, pageSize }: { pageIndex: number, pageSize: number }) {
    this.state = { ...this.state, pageNumber: pageIndex, pageSize }
    this.getDataTable();
  }

  navigateToProfile({ id }: { id: number }) {
    this._router.navigate(['../profile', id, 'view'], { relativeTo: this._route })
  }



}
