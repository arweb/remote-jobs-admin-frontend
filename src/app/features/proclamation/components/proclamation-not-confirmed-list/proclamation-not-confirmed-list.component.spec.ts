import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProclamationNotConfirmedListComponent } from './proclamation-not-confirmed-list.component';

describe('ProclamationNotConfirmedListComponent', () => {
  let component: ProclamationNotConfirmedListComponent;
  let fixture: ComponentFixture<ProclamationNotConfirmedListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProclamationNotConfirmedListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProclamationNotConfirmedListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
