import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProclamationRoutingModule } from './proclamation-routing.module';
import { ProclamationListComponent } from './components/proclamation-list/proclamation-list.component';
import { ProclamationEditedListComponent } from './components/proclamation-edited-list/proclamation-edited-list.component';
import { ProclamationNotConfirmedListComponent } from './components/proclamation-not-confirmed-list/proclamation-not-confirmed-list.component';
import { SharedModule } from '@app/shared';


@NgModule({
  declarations: [
    ProclamationListComponent,
    ProclamationEditedListComponent,
    ProclamationNotConfirmedListComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    ProclamationRoutingModule
  ]
})
export class ProclamationModule { }
