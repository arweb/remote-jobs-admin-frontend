import { Injectable } from "@angular/core";
import { DataListAndCount, DataSourceRequestState } from "@app/core";
import { ApiHttpResponse } from "@app/core/models/api-http-response.model";
import { AppCrudService } from "@app/core/services/app-crud.service";
import { map, Observable } from "rxjs";
import { ProclamationFindModel } from "../models/proclamation-find.model";

@Injectable({ providedIn: "root" })
export class ProclamationService {
    constructor(private readonly _appCrudService: AppCrudService) { }

    getNotConfrimedList(state: DataSourceRequestState): Observable<DataListAndCount<ProclamationFindModel>> {
        let params = { ...state, pageNumber: state?.pageNumber.toString(), pageSize: state?.pageSize.toString() }
        const queryString = new URLSearchParams(params);

        return this._appCrudService.getRequest(`proclamation/not-confirmed-list?${queryString}`, undefined)
            .pipe(map((response: ApiHttpResponse<DataListAndCount<ProclamationFindModel>>) => {
                return response?.data
            }))
    }
}