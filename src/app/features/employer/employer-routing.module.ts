import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployerEditedComponent } from './components/employer-edited/employer-edited.component';
import { EmployerListComponent } from './components/employer-list/employer-list.component';
import { EmployerSuspendComponent } from './components/employer-suspend/employer-suspend.component';
import { NotConfirmedHistoryViewComponent } from './components/not-confirmed-history-view/not-confirmed-history-view.component';
import { NotConfirmedViewComponent } from './components/not-confirmed-view/not-confirmed-view.component';

const routes: Routes = [
  {
    path: "", redirectTo: "active", pathMatch: "full"
  },
  {
    path: "active", component: EmployerListComponent
  },
  {
    path: "suspend", component: EmployerSuspendComponent
  },
  {
    path: "not-confirmed-view/:userId", component: NotConfirmedViewComponent
  },
  {
    path: "not-confirmed-history-view/:userId", component: NotConfirmedHistoryViewComponent
  },
  {
    path: "edited", component: EmployerEditedComponent
  },
  {
    path: "profile/:id", loadChildren: () => import('../../modules/employer-profile/employer-profile.module').then(m => m.EmployerProfileModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployerRoutingModule { }
