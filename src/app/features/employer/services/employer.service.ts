import { Injectable } from "@angular/core";
import { DataListAndCount, DataSourceRequestState } from "@app/core";
import { LoadingHttpRequest } from "@app/core/enums/show-loading-http-enum";
import { ApiHttpResponse } from "@app/core/models/api-http-response.model";
import { AppCrudService } from "@app/core/services/app-crud.service";
import { map, Observable, Subject } from "rxjs";
import { UserFilterModel } from "../models/company-filter.model";
import { CompanyFindDto } from "../models/company-find.dto";

@Injectable({ providedIn: "root" })
export class EmployerService {

    private filterSource = new Subject<UserFilterModel>()
    companyFilters$ = this.filterSource.asObservable();


    constructor(private readonly _appCrudService: AppCrudService) { }
    getDataTableActiveList(state: DataSourceRequestState): Observable<DataListAndCount<CompanyFindDto>> {

        let params = { ...state, pageNumber: state?.pageNumber.toString(), pageSize: state?.pageSize.toString() }
        const queryString = new URLSearchParams(params);

        return this._appCrudService.getRequest(`company/active-list?${queryString}`, undefined)
            .pipe(map((response: ApiHttpResponse<DataListAndCount<CompanyFindDto>>) => {
                return response?.data
            }))
    }

    getDataTableSuspendList(state: DataSourceRequestState): Observable<DataListAndCount<CompanyFindDto>> {

        let params = { pageNumber: state?.pageNumber.toString(), pageSize: state?.pageSize.toString() }
        const queryString = new URLSearchParams(params);

        return this._appCrudService.getRequest(`company/suspend-list?${queryString}`, undefined)
            .pipe(map((response: ApiHttpResponse<DataListAndCount<CompanyFindDto>>) => {
                return response?.data
            }))
    }

    getDataTableEditedList(state: DataSourceRequestState): Observable<DataListAndCount<CompanyFindDto>> {

        let params = { pageNumber: state?.pageNumber.toString(), pageSize: state?.pageSize.toString() }
        const queryString = new URLSearchParams(params);

        return this._appCrudService.getRequest(`company/edited-list?${queryString}`, undefined)
            .pipe(map((response: ApiHttpResponse<DataListAndCount<CompanyFindDto>>) => {
                return response?.data
            }))
    }


    getCompanyInfo(userId: number) {
        return this._appCrudService.getRequest(`company/company-user-info`, userId, LoadingHttpRequest.WithoutAnyLoading)
            .pipe(map((response: ApiHttpResponse<CompanyFindDto>) => {
                return response?.data
            }))
    }


    getCompanyHistoryInfo(userId: number): Observable<{ original: CompanyFindDto; temporary: CompanyFindDto; }> {
        return this._appCrudService.getRequest('company/company-user-info-history', userId, LoadingHttpRequest.WithoutAnyLoading)
            .pipe(map((response: ApiHttpResponse<{ original: CompanyFindDto; temporary: CompanyFindDto; }>) => {
                return response?.data
            }))
    }


    confirmCreatedCompanyInfo(userId: number) {
        return this._appCrudService.postRequest({}, "account/company/info/created/confirm", userId, LoadingHttpRequest.WithoutAnyLoading)
    }


    confirmEditedCompanyInfo(userId: number) {
        return this._appCrudService.postRequest({}, "account/company/info/edited/confirm", userId, LoadingHttpRequest.WithoutAnyLoading)
    }


    toggleActivationStatus(userId: number) {
        return this._appCrudService.putRequest("user/toggle-activation", {}, userId, LoadingHttpRequest.WithoutAnyLoading)
    }



    boradcasteCompanyFilter(filter: UserFilterModel) {
        this.filterSource.next(filter)
    }
}