export enum CompanyStaffCount {
    OneToTen = 1,
    ElevenToTwenty = 2,
    TwentyOneToFifty = 3,
    FiftyOneToOneHundred = 4,
    OneHundredOneToTwoHundred = 5,
    TwoHundredOntToFiveHundred = 6,
    MoreThanFiveHundred = 7
}