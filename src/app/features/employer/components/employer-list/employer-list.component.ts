import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { DataListAndCount } from '@app/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { EmployerService } from '../../services/employer.service';
import { finalize } from 'rxjs/operators';
import { CompanyFindDto } from '../../models/company-find.dto';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { EmployerFilterComponent } from '../employer-filter/employer-filter.component';
import { RjCommonService, UserStatus } from '@app/modules/rj-common';
import { SelectListItem } from '@app/shared';
import { Subscription } from 'rxjs';
import { UserFilterModel, DataSourceCompanyFilterRequest } from '../../models/company-filter.model';

@Component({
  selector: 'app-employer-list',
  templateUrl: './employer-list.component.html',
  styleUrls: ['./employer-list.component.scss']
})
export class EmployerListComponent implements OnInit, OnDestroy {
  public displayedColumns: string[] = [
    "id",
    "englishName",
    "persianName",
    "orderNo",
    "userFullName",
    "email",
    "status",
    "createdAt",
    "cellPhone",
    "action"
  ];

  public dataItems: any[] = [];
  public state!: DataSourceCompanyFilterRequest;
  public activityItems!: Array<SelectListItem>

  private subscriptionFilter!: Subscription

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    private readonly _commonService: RjCommonService,
    public readonly _dialog: MatDialog,
    private readonly _router: Router,
    private readonly _route: ActivatedRoute,
    private readonly _employerService: EmployerService,
    private readonly _spinner: NgxSpinnerService,
  ) { }


  ngOnInit(): void {
    this.init();
    this.getActivityItems();

    this.getDataTable();
    this.listenToFilterInput();
  }

  private init() {
    this.state = { pageNumber: 0, pageSize: 15, status: UserStatus.Active } as DataSourceCompanyFilterRequest;
    this.activityItems = []
  }

  private getActivityItems() {
    this._commonService.getActivityItems().subscribe((response: Array<SelectListItem>) => {
      this.activityItems = response
    })
  }


  private listenToFilterInput() {
    this.subscriptionFilter = this._employerService.companyFilters$.subscribe((filter: UserFilterModel) => {
      this.state.pageNumber = 0;
      this.state.orderNo = filter.orderNo;
      this.state.persianName = filter.persianName ?? '';
      this.state.englishName = filter.englishName ?? '';
      this.state.cellPhone = filter.cellPhone ?? '';
      this.state.fullName = filter.fullName ?? '';
      this.state.email = filter.email ?? '';
      this.state.status = filter.status ?? undefined

      this.getDataTable();
    })
  }



  private getDataTable() {
    this._spinner.show();
    this._employerService.getDataTableActiveList(this.state)
      .pipe(
        finalize(() => {
          this._spinner.hide();
        }))
      .subscribe((response: DataListAndCount<CompanyFindDto>) => {
        const { count, items } = response;
        this.paginator.length = count;
        this.dataItems = items;
      });
  }


  changePaging({ pageIndex, pageSize }: { pageIndex: number, pageSize: number }) {
    this.state = { ...this.state, pageNumber: pageIndex, pageSize }
    this.getDataTable();
  }

  navigateToProfile({ userId }: CompanyFindDto) {
    this._router.navigate(['../profile', userId], { relativeTo: this._route })
  }

  togleActivationStatus({ userId, status }: CompanyFindDto) {
    this._spinner.show();
    this._employerService.toggleActivationStatus(userId)
      .pipe(
        finalize(() => {
          this._spinner.hide();
        }))
      .subscribe(_ => {
        this.dataItems = this.dataItems.map(user => {
          if (user.userId == userId) return { ...user, status: status == UserStatus.Active ? UserStatus.Deactive : UserStatus.Active }
          return { ...user }
        })
      });
  }

  showFilterDialog() {
    this._dialog.open(EmployerFilterComponent, { data: { section: "active", filter: this.state }, disableClose: true, panelClass: 'modal-sm' });
  }
  get UserActiveStatus() { return UserStatus.Active }

  ngOnDestroy(): void {
    this.subscriptionFilter && this.subscriptionFilter.unsubscribe();
  }

}
