import { Component, Inject, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RjCommonService } from '@app/modules/rj-common';
import { SelectListItem } from '@app/shared';
import { UserFilterModel } from '../../models/company-filter.model';
import { EmployerService } from '../../services/employer.service';

@Component({
  selector: 'app-employer-filter',
  templateUrl: './employer-filter.component.html',
  styleUrls: ['./employer-filter.component.scss']
})
export class EmployerFilterComponent implements OnInit {
  public model!: UserFilterModel
  public section!: "edited" | "active" | "suspend";
  public statusItems!: Array<SelectListItem>

  constructor(
    private readonly _employerService: EmployerService,
    private readonly _dialogRef: MatDialogRef<EmployerFilterComponent>,
    private readonly _commonService: RjCommonService,
    @Inject(MAT_DIALOG_DATA) private readonly _data: { section: "edited" | "active" | "suspend", filter: UserFilterModel }
  ) { }

  ngOnInit(): void {
    this.model = this._data.filter ?? new UserFilterModel();
    this.section = this._data.section
    this.statusItems = this._commonService.getUserStatus()
  }

  submitFilterForm(form: NgForm, $event: Event) {
    $event.preventDefault();
    if (!form.valid) return;

    this._employerService.boradcasteCompanyFilter(this.model)
    this.onCloseDialog();
  }

  clearFilters() {
    this._employerService.boradcasteCompanyFilter({ ...this.model, persianName: '', englishName: '', orderNo: undefined })
    this.onCloseDialog();
  }

  onCloseDialog() {
    this._dialogRef.close()
  }

}
