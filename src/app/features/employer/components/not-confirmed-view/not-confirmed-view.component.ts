import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { RjCommonService } from '@app/modules/rj-common';
import { ConfirmDialogComponent, SelectListItem } from '@app/shared';
import { CompanyFindDto } from '../../models/company-find.dto';
import { EmployerService } from '../../services/employer.service';

@Component({
  selector: 'app-not-confirmed-view',
  templateUrl: './not-confirmed-view.component.html',
  styleUrls: ['./not-confirmed-view.component.scss']
})
export class NotConfirmedViewComponent implements OnInit {
  public model!: CompanyFindDto
  public activityItems!: Array<SelectListItem>

  constructor(
    private readonly _router: Router,
    private readonly _route: ActivatedRoute,
    private readonly _employerService: EmployerService,
    private readonly _commonService: RjCommonService,
    public readonly _dialog: MatDialog,) { }

  ngOnInit(): void {
    this.init();
    this.getActivityItems();
    this.getDataItem();
  }

  private init() {
    this.model = {} as CompanyFindDto;
    this.activityItems = []
  }

  private getActivityItems() {
    this._commonService.getActivityItems().subscribe((response: Array<SelectListItem>) => {
      this.activityItems = response
    })
  }

  private getDataItem() {
    const userId = +this._route.snapshot.params['userId']
    this._employerService.getCompanyInfo(userId).subscribe(response => {
      this.model = response
    })
  }

  openConfirmDialog() {
    const dialogRef = this._dialog.open(ConfirmDialogComponent, { data: {}, disableClose: true });
    dialogRef.afterClosed().subscribe(result => {
      const userId = +this._route.snapshot.params['userId']
      result && this._employerService.confirmCreatedCompanyInfo(userId).subscribe(_ => {
        this._router.navigate(['/employers/suspend'])
      })
    });
  }


}
