import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { RjCommonService } from '@app/modules/rj-common';
import { ConfirmDialogComponent, SelectListItem } from '@app/shared';
import { CompanyFindDto } from '../../models/company-find.dto';
import { EmployerService } from '../../services/employer.service';

@Component({
  selector: 'app-not-confirmed-history-view',
  templateUrl: './not-confirmed-history-view.component.html',
  styleUrls: ['./not-confirmed-history-view.component.scss']
})
export class NotConfirmedHistoryViewComponent implements OnInit {

  public original!: CompanyFindDto
  public temporary!: CompanyFindDto
  public activityItems!: Array<SelectListItem>

  constructor(
    private readonly _router: Router,
    private readonly _route: ActivatedRoute,
    private readonly _employerService: EmployerService,
    private readonly _commonService: RjCommonService,
    public readonly _dialog: MatDialog,) { }

  ngOnInit(): void {
    this.init();
    this.getDataItem();
    this.getActivityItems();
  }

  private init() {
    this.original = {} as CompanyFindDto;
    this.activityItems = []
  }

  private getActivityItems() {
    this._commonService.getActivityItems().subscribe((response: Array<SelectListItem>) => {
      this.activityItems = response
    })
  }

  private getDataItem() {
    const userId = +this._route.snapshot.params['userId']
    this._employerService.getCompanyHistoryInfo(userId).subscribe((response: { original: CompanyFindDto, temporary: CompanyFindDto }) => {
      this.original = response.original
      this.temporary = response.temporary
    })
  }

  openConfirmDialog() {
    const dialogRef = this._dialog.open(ConfirmDialogComponent, { data: {}, disableClose: true });
    dialogRef.afterClosed().subscribe(result => {
      const userId = +this._route.snapshot.params['userId']
      result && this._employerService.confirmEditedCompanyInfo(userId).subscribe(_ => {
        this._router.navigate(['/employers/edited'])
      })
    });
  }

}
