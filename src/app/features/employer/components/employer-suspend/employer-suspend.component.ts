import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { DataListAndCount, DataSourceRequestState } from '@app/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { EmployerService } from '../../services/employer.service';
import { finalize } from 'rxjs/operators';
import { CompanyFindDto } from '../../models/company-find.dto';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { RjCommonService } from '@app/modules/rj-common';
import { SelectListItem } from '@app/shared';
import { EmployerFilterComponent } from '../employer-filter/employer-filter.component';
import { Subscription } from 'rxjs';
import { DataSourceCompanyFilterRequest, UserFilterModel } from '../../models/company-filter.model';

@Component({
  selector: 'app-employer-suspend',
  templateUrl: './employer-suspend.component.html',
  styleUrls: ['./employer-suspend.component.scss']
})
export class EmployerSuspendComponent implements OnInit, OnDestroy {

  public displayedColumns: string[] = [
    "id",
    "englishName",
    "persianName",
    "orderNo",
    "userFullName",
    "email",
    "createdAt",
    "cellPhone",
    "action"
  ];

  public dataItems: any[] = [];
  public state!: DataSourceCompanyFilterRequest;
  public activityItems!: Array<SelectListItem>

  private subscriptionFilter!: Subscription

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    public readonly _dialog: MatDialog,
    private readonly _router: Router,
    private readonly _route: ActivatedRoute,
    private readonly _employerService: EmployerService,
    private readonly _commonService:RjCommonService,
    private readonly _spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.init();
    this.getActivityItems();

    this.getDataTable();
    this.listenToFilterInput();
  }

  private init() {
    this.state = { pageNumber: 0, pageSize: 15 } as DataSourceCompanyFilterRequest;
    this.activityItems = []
  }

  private listenToFilterInput() {
    this.subscriptionFilter = this._employerService.companyFilters$.subscribe((filter: UserFilterModel) => {
      this.state.pageNumber = 0;
      this.state.orderNo = filter.orderNo;
      this.state.persianName = filter.persianName ?? '';
      this.state.englishName = filter.englishName ?? '';
      this.state.cellPhone = filter.cellPhone ?? '';
      this.state.fullName = filter.fullName ?? '';
      this.state.email = filter.email ?? '';

      this.getDataTable();
    })
  }


  private getActivityItems() {
    this._commonService.getActivityItems().subscribe((response: Array<SelectListItem>) => {
      this.activityItems = response
    })
  }


  private getDataTable() {
    this._spinner.show();
    this._employerService.getDataTableSuspendList(this.state)
      .pipe(
        finalize(() => {
          this._spinner.hide();
        }))
      .subscribe((response: DataListAndCount<CompanyFindDto>) => {
        const { count, items } = response;
        this.paginator.length = count;
        this.dataItems = items;
      });
  }


  changePaging({ pageIndex, pageSize }: { pageIndex: number, pageSize: number }) {
    this.state = { ...this.state, pageNumber: pageIndex, pageSize }
    this.getDataTable();
  }

  navigateToNotConfirmedView({ userId }: { userId: number }) {
    this._router.navigate(['../not-confirmed-view', userId], { relativeTo: this._route })
  }


  showFilterDialog() {
    this._dialog.open(EmployerFilterComponent, { data: { section: "suspend", filter: this.state }, disableClose: true, panelClass: 'modal-sm' });
  }

  ngOnDestroy(): void {
    this.subscriptionFilter && this.subscriptionFilter.unsubscribe();
  }

}
