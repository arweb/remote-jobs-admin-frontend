import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { DataListAndCount, DataSourceRequestState } from '@app/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { EmployerService } from '../../services/employer.service';
import { finalize } from 'rxjs/operators';
import { CompanyFindDto } from '../../models/company-find.dto';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { EmployerFilterComponent } from '../employer-filter/employer-filter.component';
import { SelectListItem } from '@app/shared';
import { RjCommonService, UserStatus } from '@app/modules/rj-common';
import { DataSourceCompanyFilterRequest, UserFilterModel } from '../../models/company-filter.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-employer-edited',
  templateUrl: './employer-edited.component.html',
  styleUrls: ['./employer-edited.component.scss']
})
export class EmployerEditedComponent implements OnInit, OnDestroy {

  public displayedColumns: string[] = [
    "id",
    "englishName",
    "persianName",
    "orderNo",
    "daytimeTell",
    "numberOfStaff",
    "website",
    "categories",
    "userFullName",
    "action"
  ];
  public activityItems!: Array<SelectListItem>
  public dataItems: any[] = [];
  public state!: DataSourceCompanyFilterRequest;

  private subscriptionFilter!: Subscription

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    public readonly _dialog: MatDialog,
    private readonly _router: Router,
    private readonly _route: ActivatedRoute,
    private readonly _employerService: EmployerService,
    private readonly _commonService: RjCommonService,
    private readonly _spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.init();

    this.getActivityItems();
    this.getDataTable();
    this.listenToFilterInput();
  }

  private init() {
    this.state = { pageNumber: 0, pageSize: 15 } as DataSourceCompanyFilterRequest;
    this.activityItems = []
  }

  private getActivityItems() {
    this._commonService.getActivityItems().subscribe((response: Array<SelectListItem>) => {
      this.activityItems = response
    })
  }


  private getDataTable() {
    this._spinner.show();
    this._employerService.getDataTableEditedList(this.state)
      .pipe(
        finalize(() => {
          this._spinner.hide();
        }))
      .subscribe((response: DataListAndCount<CompanyFindDto>) => {
        const { count, items } = response;
        this.paginator.length = count;
        this.dataItems = items;
      });
  }

  private listenToFilterInput() {
    this.subscriptionFilter = this._employerService.companyFilters$.subscribe((filter: UserFilterModel) => {
      this.state.pageNumber = 0;
      this.state.orderNo = filter.orderNo;
      this.state.persianName = filter.persianName ?? '';
      this.state.englishName = filter.englishName ?? '';
      this.getDataTable();
    })
  }



  changePaging({ pageIndex, pageSize }: { pageIndex: number, pageSize: number }) {
    this.state = { ...this.state, pageNumber: pageIndex, pageSize }
    this.getDataTable();
  }

  navigateToNotConfirmedView({ userId }: { userId: number }) {
    this._router.navigate(['../not-confirmed-history-view', userId], { relativeTo: this._route })
  }

  navigateToProfile({ userId }: CompanyFindDto) {
    this._router.navigate(['../profile', userId], { relativeTo: this._route })
  }

  showFilterDialog() {
    this._dialog.open(EmployerFilterComponent, { data: { section: "edited", filter: this.state }, disableClose: true, panelClass: 'modal-sm' });
  }


  ngOnDestroy(): void {
    this.subscriptionFilter && this.subscriptionFilter.unsubscribe();
  }

}
