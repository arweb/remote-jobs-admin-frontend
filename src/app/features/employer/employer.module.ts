import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployerRoutingModule } from './employer-routing.module';
import { SharedModule } from '@app/shared';
import { EmployerListComponent } from './components/employer-list/employer-list.component';
import { EmployerFilterComponent } from './components/employer-filter/employer-filter.component';
import { EmployerSuspendComponent } from './components/employer-suspend/employer-suspend.component';
import { EmployerEditedComponent } from './components/employer-edited/employer-edited.component';
import { NotConfirmedViewComponent } from './components/not-confirmed-view/not-confirmed-view.component';
import { NotConfirmedHistoryViewComponent } from './components/not-confirmed-history-view/not-confirmed-history-view.component';
import { RjCommonModule } from '@app/modules/rj-common';


@NgModule({
  declarations: [
    EmployerListComponent,
    EmployerFilterComponent,
    EmployerSuspendComponent,
    EmployerEditedComponent,
    NotConfirmedViewComponent,
    NotConfirmedHistoryViewComponent
  ],
  imports: [
    CommonModule,
    RjCommonModule,
    SharedModule,
    EmployerRoutingModule
  ]
})
export class EmployerModule { }
