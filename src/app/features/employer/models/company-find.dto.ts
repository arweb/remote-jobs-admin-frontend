import { UserStatus } from "@app/modules/rj-common";
import { CompanyStaffCount } from "../enums/company-staff-count.enum";

export class CompanyFindDto {
    public id!: number;
    public englishName!: string;
    public persianName!: string;
    public companyNationalId!: string;
    public orderNo!: number;
    public daytimeTell!: string;
    public fileId!: string;
    public cellPhone!: string;
    public filePath!: string;
    public numberOfStaff!: CompanyStaffCount;
    public website!: string;
    public categories!: number[];
    public description!: string;
    public userFullName!: string;
    public email?: string
    public userId!: number;
    public createdAt!: Date
    public status!: UserStatus

    public constructor(init?: Partial<CompanyFindDto>) {
        Object.assign(this, init);
    }
}

