import { DataSourceRequestState } from '@app/core';

export class UserFilterModel {
    public englishName!: string
    public persianName!: string
    public orderNo?: string
    public email?: string
    public fullName?: string
    public cellPhone?: string
    public status?: number
}


export class DataSourceCompanyFilterRequest extends UserFilterModel implements DataSourceRequestState {
    public pageNumber!: number;
    public pageSize!: number;
}