import { RoleType } from "@app/core"

export class UserActionModel {
    public firstName!: string
    public lastName!: string
    public cellPhone!: number
    public password!: string
    public role!: number
    public email!: string
}