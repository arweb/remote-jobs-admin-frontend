import { Component, Inject, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RjCommonService } from '@app/modules/rj-common';
import { SelectListItem } from '@app/shared';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserActionModel } from '../../models/user.action.model';
import { UserService } from '../../services/user.service';
import { finalize } from 'rxjs/operators';


@Component({
  selector: 'app-user-action',
  templateUrl: './user-action.component.html',
  styleUrls: ['./user-action.component.scss']
})
export class UserActionComponent implements OnInit {

  public model!: UserActionModel
  public roleItems!: Array<SelectListItem>
  constructor(
    private readonly _dialogRef: MatDialogRef<UserActionComponent>,
    private readonly _commonService: RjCommonService,
    private readonly _userService: UserService,
    private readonly _spinner: NgxSpinnerService,
    @Inject(MAT_DIALOG_DATA) private readonly _data: UserActionModel
  ) { }

  ngOnInit(): void {
    this.model = this._data
    this.roleItems = this._commonService.getRoles()
  }

  submitFilterForm(form: NgForm, $event: Event) {
    $event.preventDefault();
    if (!form.valid) return;

    this._spinner.show();
    this._userService.submitUser(this.model)
      .pipe(
        finalize(() => {
          this._spinner.hide();
        }))
      .subscribe((_) => {
        this.onCloseDialog();
      });
  }

  onCloseDialog() {
    this._dialogRef.close()
  }

}
