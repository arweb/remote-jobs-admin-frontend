import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import { DataListAndCount, DataSourceRequestState } from '@app/core';
import { UserFindDto } from '@app/features/job-seeker/models/user-find.dto';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from '../../services/user.service';
import { finalize } from 'rxjs/operators';
import { UserActionModel } from '../../models/user.action.model';
import { MatDialog } from '@angular/material/dialog';
import { UserActionComponent } from '../user-action/user-action.component';
import { UserStatus } from '@app/modules/rj-common';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  public displayedColumns: string[] = [
    "id",
    "fullName",
    "email",
    "cellPhone",
    "status",
    "createdAt",
    "isCellPhoneVerified",
    "isEmailVerified",
    "action"
  ];

  public dataItems: any[] = [];
  public state!: DataSourceRequestState;

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    private readonly _router: Router,
    private readonly _route: ActivatedRoute,
    private readonly _userService: UserService,
    private readonly _spinner: NgxSpinnerService,
    public readonly _dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.init();

    this.getDataTable();
  }

  private init() {
    this.state = { pageNumber: 0, pageSize: 15 } as DataSourceRequestState;
  }


  private getDataTable() {
    this._spinner.show();
    this._userService.getDataTableList(this.state)
      .pipe(
        finalize(() => {
          this._spinner.hide();
        }))
      .subscribe((response: DataListAndCount<UserFindDto>) => {
        const { count, items } = response;
        this.paginator.length = count;
        this.dataItems = items;
      });
  }

  togleActivationStatus({ id, status }: UserFindDto) {
    this._spinner.show();
    this._userService.toggleActivationStatus(id)
      .pipe(
        finalize(() => {
          this._spinner.hide();
        }))
      .subscribe(_ => {
        this.dataItems = this.dataItems.map(user => {
          if (user.id == id) return { ...user, status: status == UserStatus.Active ? UserStatus.Deactive : UserStatus.Active }
          return { ...user }
        })
      });
  }


  actionUser() {
    const model = new UserActionModel();
    this._dialog.open(UserActionComponent, { data: model, disableClose: true, panelClass: "modal-sm" });
  }

  get UserActiveStatus() { return UserStatus.Active }
  changePaging({ pageIndex, pageSize }: { pageIndex: number, pageSize: number }) {
    this.state = { pageNumber: pageIndex, pageSize }
    this.getDataTable();
  }

}
