import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UserListComponent } from './components/user-list/user-list.component';
import { SharedModule } from '@app/shared';
import { RjCommonModule } from '@app/modules/rj-common';
import { UserActionComponent } from './components/user-action/user-action.component';


@NgModule({
  declarations: [
    UserListComponent,
    UserActionComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RjCommonModule,
    UserRoutingModule
  ]
})
export class UserModule { }
