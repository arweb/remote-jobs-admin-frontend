import { Injectable } from "@angular/core";
import { DataListAndCount, DataSourceRequestState } from "@app/core";
import { LoadingHttpRequest } from "@app/core/enums/show-loading-http-enum";
import { ApiHttpResponse } from "@app/core/models/api-http-response.model";
import { AppCrudService } from "@app/core/services/app-crud.service";
import { UserFindDto } from "@app/features/job-seeker/models/user-find.dto";
import { map, Observable } from "rxjs";
import { UserActionModel } from "../models/user.action.model";

@Injectable({ providedIn: "root" })
export class UserService {
    constructor(private readonly _appCrudService: AppCrudService) { }

    getDataTableList(state: DataSourceRequestState): Observable<DataListAndCount<UserFindDto>> {

        let params = { pageNumber: state?.pageNumber.toString(), pageSize: state?.pageSize.toString() }
        const queryString = new URLSearchParams(params);

        return this._appCrudService.getRequest(`user/user-list?${queryString}`, undefined)
            .pipe(map((response: ApiHttpResponse<DataListAndCount<UserFindDto>>) => {
                return response?.data
            }))
    }


    submitUser(model: UserActionModel) {
        return this._appCrudService.postRequest(model, "user/user-action", undefined, LoadingHttpRequest.WithoutAnyLoading)
    }

    toggleActivationStatus(userId: number) {
        return this._appCrudService.putRequest("user/toggle-activation", {}, userId, LoadingHttpRequest.WithoutAnyLoading)
    }
    
}