import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from '@app/shared';
import { DashboardIndexComponent } from './components/dashboard-index/dashboard-index.component';

const routes: Routes = [
  {
    path: "", component: DashboardIndexComponent, pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
