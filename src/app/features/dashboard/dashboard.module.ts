import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardIndexComponent } from './components/dashboard-index/dashboard-index.component';
import { MyMaterialModule } from '@app/modules/my-material';
import { SharedModule } from '@app/shared';


@NgModule({
  declarations: [
    DashboardIndexComponent
  ],
  imports: [
    CommonModule,
    MyMaterialModule,
    SharedModule,
    DashboardRoutingModule
  ]
})
export class DashboardModule { }
