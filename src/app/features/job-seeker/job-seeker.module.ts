import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JobSeekerRoutingModule } from './job-seeker-routing.module';
import { JobSeekerListComponent } from './components/job-seeker-list/job-seeker-list.component';
import { SharedModule } from '@app/shared';
import { RjCommonModule } from '@app/modules/rj-common';
import { JobSeekerFilterComponent } from './components/job-seeker-filter/job-seeker-filter.component';


@NgModule({
  declarations: [
    JobSeekerListComponent,
    JobSeekerFilterComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RjCommonModule,
    JobSeekerRoutingModule
  ]
})
export class JobSeekerModule { }
