import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobSeekerFilterComponent } from './job-seeker-filter.component';

describe('JobSeekerFilterComponent', () => {
  let component: JobSeekerFilterComponent;
  let fixture: ComponentFixture<JobSeekerFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobSeekerFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobSeekerFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
