import { Component, Inject, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { JobSeekerFilterModel } from '../../models/job-seeker-filter.model';
import { JobSeekerService } from '../../services/job-seeker.service';

@Component({
  selector: 'app-job-seeker-filter',
  templateUrl: './job-seeker-filter.component.html',
  styleUrls: ['./job-seeker-filter.component.scss']
})
export class JobSeekerFilterComponent implements OnInit {
  public model!: JobSeekerFilterModel
  constructor(
    private readonly _jobSeekerService: JobSeekerService,
    private readonly _dialogRef: MatDialogRef<JobSeekerFilterComponent>,
    @Inject(MAT_DIALOG_DATA) private readonly _data: JobSeekerFilterModel
  ) { }

  ngOnInit(): void {
    this.model = this._data ?? new JobSeekerFilterModel();
  }

  submitFilterForm(form: NgForm, $event: Event) {
    $event.preventDefault();
    if (!form.valid) return;

    this._jobSeekerService.boradcasteJobSeekerFilter(this.model)
    this.onCloseDialog();
  }

  clearFilters() {
    this._jobSeekerService.boradcasteJobSeekerFilter({ ...this.model })
    this.onCloseDialog();
  }

  onCloseDialog() {
    this._dialogRef.close()
  }

}
