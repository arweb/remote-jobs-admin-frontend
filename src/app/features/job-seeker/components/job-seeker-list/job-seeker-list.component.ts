import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { DataListAndCount, DataSourceRequestState } from '@app/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { JobSeekerService } from '../../services/job-seeker.service';
import { UserFindDto } from '../../models/user-find.dto';
import { JobSeekerFilterComponent } from '../job-seeker-filter/job-seeker-filter.component';
import { Subscription } from 'rxjs';
import { DataSourceJobSeekerFilterRequest, JobSeekerFilterModel } from '../../models/job-seeker-filter.model';
import { UserStatus } from '@app/modules/rj-common';

@Component({
  selector: 'app-job-seeker-list',
  templateUrl: './job-seeker-list.component.html',
  styleUrls: ['./job-seeker-list.component.scss']
})
export class JobSeekerListComponent implements OnInit {
  public displayedColumns: string[] = [
    "id",
    "fullName",
    "email",
    "cellPhone",
    "status",
    "createdAt",
    "isCellPhoneVerified",
    "isEmailVerified",
    "action"
  ];

  public dataItems: UserFindDto[] = [];
  public state!: DataSourceJobSeekerFilterRequest;
  private subscriptionFilter!: Subscription

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    public readonly _dialog: MatDialog,
    private readonly _router: Router,
    private readonly _route: ActivatedRoute,
    private readonly _jobSeekerService: JobSeekerService,
    private readonly _spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.init();

    this.getDataTable();
    this.listenToFilterInput();
  }

  private init() {
    this.state = { pageNumber: 0, pageSize: 15 } as DataSourceJobSeekerFilterRequest;
  }


  private listenToFilterInput() {
    this.subscriptionFilter = this._jobSeekerService.jobSeekerFilters$.subscribe((filter: JobSeekerFilterModel) => {
      this.state.pageNumber = 0;
      this.state.cellPhone = filter.cellPhone ?? '';
      this.state.fullName = filter.fullName ?? '';
      this.state.email = filter.email ?? '';

      this.getDataTable();
    })
  }



  private getDataTable() {
    this._spinner.show();
    this._jobSeekerService.getDataTableList(this.state)
      .pipe(
        finalize(() => {
          this._spinner.hide();
        }))
      .subscribe((response: DataListAndCount<UserFindDto>) => {
        const { count, items } = response;
        this.paginator.length = count;
        this.dataItems = items;
      });
  }


  changePaging({ pageIndex, pageSize }: { pageIndex: number, pageSize: number }) {
    this.state = { ...this.state, pageNumber: pageIndex, pageSize }
    this.getDataTable();
  }

  showFilterDialog() {
    this._dialog.open(JobSeekerFilterComponent, { data: this.state, disableClose: true, panelClass: 'modal-sm' });
  }

  get UserActiveStatus() { return UserStatus.Active }

  togleActivationStatus({ id, status }: UserFindDto) {
    this._spinner.show();
    this._jobSeekerService.toggleActivationStatus(id)
      .pipe(
        finalize(() => {
          this._spinner.hide();
        }))
      .subscribe(_ => {
        this.dataItems = this.dataItems.map(user => {
          if (user.id == id) return { ...user, status: status == UserStatus.Active ? UserStatus.Deactive : UserStatus.Active }
          return { ...user }
        })
      });
  }

  ngOnDestroy(): void {
    this.subscriptionFilter && this.subscriptionFilter.unsubscribe();
  }
}
