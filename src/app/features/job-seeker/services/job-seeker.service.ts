import { Injectable } from "@angular/core";
import { DataListAndCount, DataSourceRequestState } from "@app/core";
import { LoadingHttpRequest } from "@app/core/enums/show-loading-http-enum";
import { ApiHttpResponse } from "@app/core/models/api-http-response.model";
import { AppCrudService } from "@app/core/services/app-crud.service";
import { map, Observable, Subject } from "rxjs";
import { JobSeekerFilterModel } from "../models/job-seeker-filter.model";
import { UserFindDto } from "../models/user-find.dto";

@Injectable({ providedIn: "root" })
export class JobSeekerService {
    private filterSource = new Subject<JobSeekerFilterModel>()
    jobSeekerFilters$ = this.filterSource.asObservable();

    constructor(private readonly _appCrudService: AppCrudService) { }


    getDataTableList(state: DataSourceRequestState): Observable<DataListAndCount<UserFindDto>> {

        let params = { ...state, pageNumber: state?.pageNumber.toString(), pageSize: state?.pageSize.toString() }
        const queryString = new URLSearchParams(params);

        return this._appCrudService.getRequest(`job-seeker/list-active?${queryString}`, undefined)
            .pipe(map((response: ApiHttpResponse<DataListAndCount<UserFindDto>>) => {
                return response?.data
            }))
    }

    boradcasteJobSeekerFilter(filter: JobSeekerFilterModel) {
        this.filterSource.next(filter)
    }


    toggleActivationStatus(userId: number) {
        return this._appCrudService.putRequest("user/toggle-activation", {}, userId, LoadingHttpRequest.WithoutAnyLoading)
    }
}