import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JobSeekerListComponent } from './components/job-seeker-list/job-seeker-list.component';

const routes: Routes = [
  {
    path: "", component: JobSeekerListComponent, pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobSeekerRoutingModule { }
