export class UserFindDto {
    public id!: number;
    public fullName!: string;
    public email!: string;
    public cellPhone!: string;
    public avatar!: string;
    public role!: number;
    public type!: number;
    public status!: number;
    public createdAt!: Date;
    public isCellPhoneVerified!: boolean;
    public isEmailVerified!: boolean;
    public isActive!: boolean;
    
    public constructor(init?: Partial<UserFindDto>) {
        Object.assign(this, init);
    }
}