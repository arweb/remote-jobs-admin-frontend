import { DataSourceRequestState } from '@app/core';

export class JobSeekerFilterModel {
    public email?: string
    public fullName?: string
    public cellPhone?: string
}


export class DataSourceJobSeekerFilterRequest extends JobSeekerFilterModel implements DataSourceRequestState {
    public pageNumber!: number;
    public pageSize!: number;
}